//
//  BIOChallengeInfo.h
//  BiometricSDK
//
//  Created by RIBEIRO Tiago on 03/02/17.
//  Copyright © 2017 MORPHO. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BIOChallengeInfo : NSObject

@property (nonatomic, readonly) NSInteger currentChallengeNumber;
@property (nonatomic, readonly) NSInteger totalChallenges;

- (instancetype _Nonnull)initWithCurrentChallengeNumber:(NSInteger)currentChallengeNumber forTotal:(NSInteger)totalChallenges;

@end
