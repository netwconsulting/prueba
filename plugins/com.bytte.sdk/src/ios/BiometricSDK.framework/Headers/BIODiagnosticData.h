//
//  BIODiagnosticData.h
//  BiometricSDK
//
//  Created by RIBEIRO Tiago on 28/07/2017.
//  Copyright © 2017 MORPHO. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <BiometricSDK/BIODiagnosticEvent.h>
#import <BiometricSDK/BIOEnvironment.h>
#import <BiometricSDK/BIOSmartBioSDKInfo.h>

@interface BIODiagnosticData : NSObject <BIONSDictionaryAdapter>

@property (nonatomic, copy) NSArray<id<BIODiagnosticEvent>> * _Nonnull events;
@property (nonatomic, readonly) BIOEnvironment * _Nonnull environment;
@property (nonatomic, readonly) BIOSmartBioSDKInfo * _Nonnull smartBioSDKInfo;

@end
