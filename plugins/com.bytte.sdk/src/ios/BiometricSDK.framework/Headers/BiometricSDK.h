//
//  BiometricSDK.h
//  BiometricSDK
//
//  Created by WASAG Kamil - EXT-REPERIO (MORPHO) on 04/04/2016.
//  Copyright © 2016 WASAG Kamil - EXT-REPERIO (MORPHO). All rights reserved.
//
#import <Foundation/Foundation.h>
//! Project version number for BiometricSDK.
FOUNDATION_EXPORT double BiometricSDKVersionNumber;

//! Project version string for BiometricSDK.
FOUNDATION_EXPORT const unsigned char BiometricSDKVersionString[];
// In this header, you should import all the public headers of your framework using statements like #import <BiometricSDK/PublicHeader.h>


#import <BiometricSDK/BIOSDK.h>

//Capture Handler
#import <BiometricSDK/IBIOCaptureHandler.h>
#import <BiometricSDK/BIOTrackingInfo.h>
#import <BiometricSDK/BIOCamera.h>
#import <BiometricSDK/BIOOverlay.h>
#import <BiometricSDK/BIOTorch.h>
#import <BiometricSDK/BIOEye.h>
#import <BiometricSDK/BIOColorSpace.h>
#import <BiometricSDK/BIOCapturingError.h>
#import <BiometricSDK/BIOCaptureHandlerError.h>
#import <BiometricSDK/BIORecordingError.h>
#import <BiometricSDK/BIOCapturingInfo.h>
#import <BiometricSDK/BIODocInfo.h>
#import <BiometricSDK/BIODocumentTrackingInformation.h>
#import <BiometricSDK/BIOPreviewColorspace.h>
#import <BiometricSDK/BIOReplayProtocol.h>

//Matcher Handler
#import <BiometricSDK/BIOAuthenticationResult.h>
#import <BiometricSDK/BIOIdentificationResult.h>
#import <BiometricSDK/IBIOMatcherHandler.h>
#import <BiometricSDK/BIOMatchingCandidate.h>
#import <BiometricSDK/BIOTemplate.h>
#import <BiometricSDK/BIOMatcherHandlerError.h>
#import <BiometricSDK/BIODetectBiometricOptions.h>
#import <BiometricSDK/BIOIdentificationOptions.h>
#import <BiometricSDK/BIOMatchingOptions.h>
#import <BiometricSDK/BIOMatcherHandlerOptions.h>
#import <BiometricSDK/BIOBiometricCandidate.h>
#import <BiometricSDK/BIOBiometricReference.h>

//Common
#import <BiometricSDK/BIOBiometrics.h>
#import <BiometricSDK/BIOImage.h>
#import <BiometricSDK/BIOLocation.h>
#import <BiometricSDK/BIOModality.h>
#import <BiometricSDK/BIOLogLevel.h>

//License
#import <BiometricSDK/BIOLicense.h>
#import <BiometricSDK/BIOLicenseOptions.h>
#import <BiometricSDK/BIOLicenseStatus.h>
#import <BiometricSDK/BIOLicenseManager.h>

//BIOStore
#import <BiometricSDK/BIOStore.h>
#import <BiometricSDK/BIOUser.h>

//Utils
#import <BiometricSDK/BIOImage+ImageGetters.h>
#import <BiometricSDK/UIImage+BIOImage.h>
#import <BiometricSDK/BIOImage+BIOResize.h>
#import <BiometricSDK/BIOImage+RegionCropping.h>

//Document
#import <BiometricSDK/IDocumentCaptureHandler.h>
#import <BiometricSDK/DocumentMRZLine.h>

//Face
#import <BiometricSDK/FaceCaptureMode.h>
#import <BiometricSDK/FaceCaptureOptions.h>
#import <BiometricSDK/IFaceCaptureHandler.h>
#import <BiometricSDK/FaceCaptureHandlerDelegate.h>

//Fingerprint
#import <BiometricSDK/FingerCaptureMode.h>
#import <BiometricSDK/FingerCaptureOptions.h>
#import <BiometricSDK/IFingerCaptureHandler.h>
#import <BiometricSDK/FingerCaptureHandlerDelegate.h>

//MRZParser
#import <BiometricSDK/MRZStringConvertible.h>
#import <BiometricSDK/MRZRecord.h>
#import <BiometricSDK/FrenchIDCard.h>
#import <BiometricSDK/MRP.h>
#import <BiometricSDK/MrtdTd1.h>
#import <BiometricSDK/MrtdTd2.h>
#import <BiometricSDK/MrvA.h>
#import <BiometricSDK/MrvB.h>
#import <BiometricSDK/SlovakId2x34.h>


#import <BiometricSDK/BIODiagnosticManagerDelegate.h>
#import <BiometricSDK/BIODiagnosticData.h>
#import <BiometricSDK/BIODiagnosticEvent.h>
#import <BiometricSDK/BIODiagnosticManager.h>



