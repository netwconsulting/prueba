//
//  CameraFrontViewController.h
//  BytteLibrarySDK
//
//  Created by Allan Diaz on 8/28/17.
//  Copyright © 2017 Allan Diaz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FrontDocumentResponse.h"

@protocol CapturaFrontDocumentDelegate <NSObject>
@optional
- (void)doCapturaFrontDocumentDelegate:(FrontDocumentResponse *)frontResponse
                          withPassword:(NSString *)filePassword;
@end

@interface CameraFrontViewController : UIViewController
@property short      timeOutController;

- (id)initWithKey:(NSString *)Key AndUIImageGuide:(UIImage *) imGuiaBase;
- (void)setSerialKey:(NSString *)serialKey WithImGuia:(UIImage *) imGuiaBase;
- (void)setFilePassword:(NSString *) password;
@property (strong, nonatomic) id <CapturaFrontDocumentDelegate> frontDocumentProcessDelegate;
@end
