//
//  BioSDKFingerprintViewController.h
//  BytteLibrarySDK
//
//  Created by Allan Diaz on 8/10/17.
//  Copyright © 2017 Allan Diaz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FingerprintResult.h"
#import "FingerprintCaptureResult.h"

@protocol FingerprintProcessDelegate <NSObject>
@optional
- (void)doFingerprintProcessDelegate:(FingerprintCaptureResult *)fingerprintCapture
                        withPassword:(NSString *)filePassword;
@end


@interface BioSDKFingerprintViewController : UIViewController
@property int        numeroDedoCaptura;
@property short      timeOutController;
@property UIImage *  plantillaHuellaDerecha;
@property UIImage *  plantillaHuellaIzquierda;

-(void) setFilePassword:(NSString *) password;
//Referencia al Delegado
@property (strong, nonatomic) id <FingerprintProcessDelegate> fingerprintProcessDelegate;


@end
