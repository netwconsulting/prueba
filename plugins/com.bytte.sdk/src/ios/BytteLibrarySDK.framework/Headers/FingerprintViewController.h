//
//  FingerprintViewController.h
//  BytteLibrarySDK
//
//  Created by Allan Diaz on 12/20/16.
//  Copyright © 2016 Allan Diaz. All rights reserved.
//
#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>
#import <MediaPlayer/MediaPlayer.h>
#import <UIKit/UIKit.h>
#import "InfoDocumento.h"


//Define el delegado para manejo de eventos


@interface FingerprintViewController : UIViewController

- (id)initGuia:(UIImage *) imgGuia;
- (void) viewControllerLoad;
- (void)iniciaCapturaHuella;

@property int numeroHuellasCaptura;
@property int numeroDedo;
@property (strong, nonatomic)  UIViewController * parentViewController;
@end
