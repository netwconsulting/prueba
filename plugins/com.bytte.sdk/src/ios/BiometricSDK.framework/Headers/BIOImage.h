//
//  BIOImage.h
//  BiometricSDK
//
//  Created by WASAG Kamil - EXT-REPERIO (MORPHO) on 11/04/2016.
//  Copyright © 2016 WASAG Kamil - EXT-REPERIO (MORPHO). All rights reserved.
//

#import <Foundation/Foundation.h>
#import <BiometricSDK/BIOBiometrics.h>
#import <BiometricSDK/BIOColorSpace.h>

/**
 * BIOImage contains all information about capturet image
 */
@interface BIOImage : BIOBiometrics

/**
 * Contains binary data of image
 */
@property (nonatomic, strong) NSData* buffer;

/**
 * Size in bytes to go from one line to the next
 */
@property (nonatomic) int stride;

/**
 * Width of the image
 */
@property (nonatomic) uint32_t width;

/**
 * Height of the image
 */
@property (nonatomic) uint32_t height;

/**
 * Image ColorSpace
 */
@property (nonatomic) BIOColorSpace colorSpace;

/**
 * Resolution in DPI. Set to +0.0F when unknown
 */
@property (nonatomic) float resolution;


@property (nonatomic, getter=isAlive) BOOL alive;
/**
 * Quality of image
 */
@property (nonatomic) int imageQuality;

@end