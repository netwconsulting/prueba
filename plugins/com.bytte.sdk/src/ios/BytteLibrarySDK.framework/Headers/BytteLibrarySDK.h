//
//  BytteLibrarySDK.h
//  BytteLibrarySDK
//
//  Created by Allan Diaz on 12/19/16.
//  Copyright © 2016 Allan Diaz. All rights reserved.
//


#import <UIKit/UIKit.h>
#import "InfoDocumento.h"
#import "BaseResponse.h"
#import "FaceResponse.h"
#import "DocumentoInfoResponse.h"
#import "CameraViewController.h"
#import "CameraFrontViewController.h"
#import "FingerprintViewController.h"
#import "FingerprintBaseViewController.h"
#import "FingerprintResult.h"
#import "SendDataProcess.h"
#import "Util.h"
#import "BioSDKLicence.h"
#import "BioSDKFaceViewController.h"
#import "BioSDKFingerprintViewController.h"
#import "FingerprintCaptureResult.h"
#import "OCRDocumentoFrente.h"
#import "OCRDocumentoReverso.h"
#import "OCRResponse.h"
#import "AutenticacionDocumentoResponse.h"
#import "LicenseMorphoResponse.h"
#import "ChileFrontViewController.h"
#import "ChileBackViewController.h"
#import "InfoChileDocumento.h"
#import "MBSSUserResponse.h"

//! Project version number for BytteLibrarySDK.
FOUNDATION_EXPORT double BytteLibrarySDKVersionNumber;

//! Project version string for BytteLibrarySDK.
FOUNDATION_EXPORT const unsigned char BytteLibrarySDKVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <BytteLibrarySDK/PublicHeader.h>


