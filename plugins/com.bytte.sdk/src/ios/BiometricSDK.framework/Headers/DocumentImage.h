//
//  DocumentImage.h
//  BiometricSDK
//
//  Created by WASAG Kamil - EXT-REPERIO (MORPHO) on 30/05/2016.
//  Copyright © 2016 WASAG Kamil - EXT-REPERIO (MORPHO). All rights reserved.
//

#import <BiometricSDK/BIOImage.h>
#import <BiometricSDK/BIODocumentTrackingInformation.h>

@interface DocumentImage : BIOImage

- (instancetype _Nonnull)initWithBIOImage:(BIOImage* _Nonnull)bioImage;

/**
 *  @author Morpho
 *
 *  @brief Give sharpnes in percentage
 */
@property (nonatomic) NSInteger sharpnessPercentage;

/**
 *  @author Morpho
 *
 *  @brief Gives a ratio (percentage) concerning the document completeness
 */
@property (nonatomic) NSInteger integrityPercentage;

/**
 *  @author Morpho
 *
 *  @brief Gives a status about Reflection detected
 */
@property (nonatomic) NSInteger qualityReflection;


/**
 *  @author Morpho
 *
 *  @brief Gives the location of this image in the document
 */
@property (nonatomic) BIODocLocation location;

/**
 *  @author Morpho
 *
 *  @brief The Coordinates of the document within the image. It can be null.
 */
@property (nonatomic, strong , nullable) NSArray<BIODocumentTrackingInformation*> *trackingInfo;

@end
