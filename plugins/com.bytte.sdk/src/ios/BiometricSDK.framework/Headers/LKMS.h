//
//  LKMS.h
//  LKMS
//
//  Created by WASAG Kamil - EXT-REPERIO (MORPHO) on 10/06/2016.
//  Copyright © 2016 MORPHO. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for LKMS.
FOUNDATION_EXPORT double LKMSVersionNumber;

//! Project version string for LKMS.
FOUNDATION_EXPORT const unsigned char LKMSVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <LKMS/PublicHeader.h>
#import "LKMSLicense.h"
#import "LKMSTerminalService.h"