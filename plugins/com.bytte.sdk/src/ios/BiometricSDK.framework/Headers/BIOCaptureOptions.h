//
//  BCBioCaptureOptions.h
//  BiometricSDK
//
//  Created by WASAG Kamil - EXT-REPERIO (MORPHO) on 04/04/2016.
//  Copyright © 2016 WASAG Kamil - EXT-REPERIO (MORPHO). All rights reserved.
//

#import <BiometricSDK/BIOMode.h>


/**
 *  BCBioCaptureOptions contains all configuration information for capture session in BCBioCaptureHandler
 */

@interface BIOCaptureOptions : BIOMode


/**
 *  @author Morpho
 *
 *  @brief Property that sets colorspace
 */
@property (nonatomic, assign) BIOPreviewColorspace previewColorspace;

@end
