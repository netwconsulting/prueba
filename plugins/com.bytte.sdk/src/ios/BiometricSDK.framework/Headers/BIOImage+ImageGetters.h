//
//  BIOImage+ImageGetters.h
//  BiometricSDK
//
//  Created by WASAG Kamil - EXT-REPERIO (MORPHO) on 22/04/2016.
//  Copyright © 2016 WASAG Kamil - EXT-REPERIO (MORPHO). All rights reserved.
//

#import <BiometricSDK/BIOImage.h>

@interface BIOImage (ImageGetters)

/**
 *  Method convert UIImage to BIOImage
 *
 *  @param image UIImage to convert to BIOImage. NOTE: Method does not check if on image is readable face.
 *
 *  @return BIOImage crated from given UIImage
 */

+ (BIOImage* _Nonnull)BIOImageFromUIImage:(UIImage* _Nonnull)image;

@end
