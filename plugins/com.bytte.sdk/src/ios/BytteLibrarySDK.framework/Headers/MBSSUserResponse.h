//
//  MBSSUserResponse.h
//  BytteLibrarySDK
//
//  Created by Allan Diaz on 12/19/17.
//  Copyright © 2017 Allan Diaz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import "BaseResponse.h"

@interface MBSSUserResponse : BaseResponse

@property (nonatomic) bool PersonExist;
@property (nonatomic) bool HasFaceTemplate;
@property (nonatomic) bool HasFingerprintTemplate;
@property (nonatomic) bool HasIrisTemplate;
@property (nonatomic) bool HasPalmTemplate;

-(NSString *) Convert2JSONValue;

@end
