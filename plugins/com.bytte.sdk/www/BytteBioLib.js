/*
 ||---------------------------------------------------------------------
 BytteBioLib - Objective C         - Xcode              Allan Diaz :)
 BytteBioLib - Android Java        - Android Studio     Venancio Prada
 BytteBioLib - JavaScript Cordova  - Visual Studio Code Allan Diaz
 
 //Inicio el Genesis
 Noviembre de 2016
 
 Script con las funciones requeridas para implementar el
 SDK de Bytte Biometrico y Captura codigo de barras desde
 dispositivos IOS Iphone
 Bytte 2016
 
 ||-------------------------------------------------------
 Enero 2017
 Se adiciona la funcionalidad de envio y cotejo del
 lado del servidor
 Allan Diaz
 ||-------------------------------------------------------
 Febrero de 2017
 Se corrige la toma del Documento
 Se corrige la ubicacion de la camara para Portrait
 ||-------------------------------------------------------
 Allan Diaz
 ||-------------------------------------------------------
 Julio de 2017
 Se corrige la captura en dispositivos + (Iphone Plus 6 y 7)
 Se corrige el tiempo de captura en dispositivos con procesador A9
 Se homologa la captura de huella(s) al concepto de Android y se remueven los circulos
 ||-------------------------------------------------------
 Septiembre de 2017
 Se Adiciona la Fncionalidad de Rostro y Captura Frente del Documento!
 ||-------------------------------------------------------
 Octubre de 2017
 Se Adiciona la Fncionalidad para ofuscar archivos con clave!!
 Se Adiciona la Funcionalidad para autenticacion de documento sin rostro
 ||-------------------------------------------------------
 Noviembre de 2017
 Se adiciona la validacion de documento (Frente y reverso) con rostro unicamente
 Se adiciona la validacion de Match facial (Rostro vivo Vs Captura) (Basico)
 */

var exec = require('cordova/exec');

//Funcion de Test
exports.echo = function(arg0, success, error) {
    exec(success, error, "BytteBioLib", "echo", [arg0]);
};

//Inicia la Licencia del componente Morpho
exports.startMorphoLicense = function(success, error) {
    exec(success, error, "BytteBioLib", "startMorphoLicense", ['']);
};

//Inicia la Captura de la imagen y Codigo de Barras!
//arg0 Codigo Activacion Licencia
//arg1 KeyPass
//arg2 TimeOut
exports.startBarCode = function(arg0, arg1, arg2 , success, error) {
    exec(success, error, "BytteBioLib", "startBarCode", [arg0, arg1, arg2]);
};

//Inicia la Captura del Frente del Documento!
//arg0 Codigo Activacion Licencia
//arg1 KeyPass
//arg2 TimeOut
exports.startFrontDocument = function(arg0, arg1, arg2, success, error) {
    exec(success, error, "BytteBioLib", "startFrontDocument", [arg0, arg1, arg2]);
};

//Inicia la Captura del Selfie
//arg0 Minucia para Autenticacion
//arg1 KeyPass
//arg2 TimeOut
//arg3 FaceCaptureMode
exports.startFaceCapture = function(arg0, arg1, arg2, arg3, success, error) {
    exec(success, error, "BytteBioLib", "startFaceCapture", [arg0, arg1, arg2, arg3]);
};

//Inicia la Captura de la huella
//arg0 Numero de Dedo (1 al 10)
//arg1 Numero de Capturas
//arg2 KeyPass
//arg3 TimeOut 
exports.startFingerprint = function(arg0, arg1, arg2, arg3, success, error) {
    exec(success, error, "BytteBioLib", "startFingerprint", [arg0, arg1, arg2, arg3]);
};

//Envia la Informacion al Servidor para su Cotejo
//arg0 Id del Proceso
//arg1 Path del Documento
//arg2 Codigo de barras en Base64
//arg3 Path del WSQ #1
//arg4 Path del WSQ #2
exports.sendDataProcess = function(arg0, arg1, arg2, arg3, arg4, success, error) {
    exec(success, error, "BytteBioLib", "sendDataProcess", [arg0, arg1, arg2, arg3, arg4]);
};

//Envia la Informacion al Servidor para su Cotejo con las Imagenes Morpho
//arg0 Id del Proceso
//arg1 Path del Documento
//arg2 Codigo de barras en Base64
//arg3 Path Imagen 1 Capturada
//arg4 Path Imagen 2 Capturada
//arg5 Path Imagen 3 Capturada
//arg6 Path Imagen 4 Capturada
//arg7 NumeroDocumento
exports.sendFingerprintProcess = function(arg0, arg1, arg2, arg3, arg4, arg5, arg6, arg7, success, error) {
    exec(success, error, 
                  "BytteBioLib", 
                  "sendFingerprintProcess", 
                  [arg0, arg1, arg2, arg3, arg4, arg5, arg6, arg7]);
};

//Envia la informacion al Servidor para el Match entre el face capturado
//Con el SDK de Bytte y el frente del documento
//arg0 Id del Proceso
//arg1 Path del Frente del Documento
//arg2 Path del Selfie
exports.sendFaceProcess = function(arg0, arg1, arg2, success, error) {
    exec(success, error, 
                  "BytteBioLib", 
                  "sendFaceProcess", 
                  [arg0, arg1, arg2]);
};

//Envia la informacion al Servidor para procesamiento de OCR de las imagenes
//arg0 :  Id del Proceso
//arg1 :  Imagen del Frente del Documento
//arg2 :  Imagen del Reverso del Documento
exports.sendOCRProcess = function(arg0, arg1, arg2, success, error) {
    exec(success, error, 
                  "BytteBioLib", 
                  "sendOCRProcess", 
                  [arg0, arg1, arg2]);
};

//Proceso de Envio de Informacion al Servidor
//Para Autenticacion del Documento
//arg0  :  Id del Proceso
//arg1  :  NumeroDocumento
//arg2  :  Imagen del Frente del Documento
//arg3  :  Imagen del Reverso del Documento
//arg4  :  BarCodeBase64
//arg5  :  Huella #1
//arg6  :  Huella #2
//arg7  :  Huella #3
//arg8  :  Huella #4
//arg9  :  FilePwd -> Password con que se ofuscaron las imagenes
//arg10 :  Version -> 2
exports.sendAutenticacionDocumentoProcess = function(arg0, arg1, arg2, arg3, arg4, arg5,
                                                     arg6, arg7, arg8, arg9, arg10, 
                                                     success, error) {
    exec(success, error, 
                  "BytteBioLib", 
                  "sendAutenticacionDocumentoProcess", 
                  [arg0, arg1, arg2,arg3, arg4, arg5, arg6, arg7, arg8, arg9, arg10]);
};


//Match Facial unicamente
//Proceso de Envio de Informacion al Servidor
//Para Match facial del Documento Vs Rostro del usuario
//arg0  :  Id del Proceso
//arg1  :  NumeroDocumento
//arg2  :  Imagen del Frente del Documento
//arg3  :  Path Imagen Selfie Usuario
//arg4  :  FilePwd -> Password con que se ofuscaron las imagenes
//arg5  :  Version -> 1
exports.sendMatchDocumentoFaceProcess = function(arg0, arg1, arg2, arg3, arg4, arg5,
                                                     success, error) {
    exec(success, error, 
    "BytteBioLib", 
    "sendMatchDocumentoFaceProcess", 
    [arg0, arg1, arg2,arg3, arg4, arg5]);
};

//Proceso de Envio de Informacion al Servidor
//Para Autenticacion del Documento Face
//arg0  :  Id del Proceso
//arg1  :  NumeroDocumento
//arg2  :  Imagen del Frente del Documento
//arg3  :  Imagen del Reverso del Documento
//arg4  :  BarCodeBase64
//arg5  :  Path de la Selfie del Usuario
//arg6  :  FilePwd -> Password con que se ofuscaron las imagenes
//arg7  :  Version -> 1
exports.sendAutenticacionDocumentoFaceProcess = function(arg0, arg1, arg2, arg3, arg4, arg5,
                                                        arg6, arg7, success, error) {
    exec(success, error, 
    "BytteBioLib", 
    "sendAutenticacionDocumentoFaceProcess", 
    [arg0, arg1, arg2,arg3, arg4, arg5, arg6, arg7]);
};