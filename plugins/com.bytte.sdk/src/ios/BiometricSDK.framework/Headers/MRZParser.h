//
//  MRZParser.h
//  BiometricSDK
//
//  Created by RIBEIRO Tiago on 28/04/17.
//  Copyright © 2017 MORPHO. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <BiometricSDK/MRZRecord.h>
#import <BiometricSDK/MRZRange.h>
#import <BiometricSDK/MRZDate.h>
#import <BiometricSDK/MRZSex.h>

@interface MRZParser : NSObject

- (instancetype __nonnull)initWihtMRZ:(NSString * __nonnull)mrzString;

- (NSString * __nonnull)rawValue:(MRZRange * __nonnull)range;
- (NSString * __nonnull)rawValues:(MRZRange * __nullable)range, ... NS_REQUIRES_NIL_TERMINATION;
- (NSArray<NSString *> * __nullable)parseName:(MRZRange * __nonnull)range error:(NSError * __nullable * __nullable)error;
- (NSString * __nullable)parseString:(MRZRange * __nonnull)range error:(NSError * __nullable * __nullable)error;
- (MRZDate * __nullable)parseDate:(MRZRange * __nonnull)range error:(NSError * __nullable * __nullable)error;
- (MRZSex)parseSexAtCol:(int)col row:(int)row error:(NSError * __nullable * __nullable)error;
- (NSError * __nullable)checkDigitForCol:(int)col row:(int)row mrzRange:(MRZRange * __nonnull)mrzRange andFieldName:(NSString * __nonnull)fieldName;
- (NSError * __nullable)checkDigitForCol:(int)col row:(int)row mrz:(NSString * __nonnull)mrz andFieldName:(NSString * __nonnull)fieldName;

+ (MRZRecord * __nullable)parse:(NSString * __nonnull)mrzString error:(NSError * __nullable * __nullable)error;


@end
