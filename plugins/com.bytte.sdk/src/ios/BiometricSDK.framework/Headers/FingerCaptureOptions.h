//
//  FingerprintCaptureOptions.h
//  BiometricSDK
//
//  Created by RIBEIRO Tiago on 16/03/17.
//  Copyright © 2017 MORPHO. All rights reserved.
//

#import <BiometricSDK/BIOCaptureOptions.h>
#import <BiometricSDK/FingerCaptureMode.h>
#import <BiometricSDK/BIOTemplate.h>

@interface FingerCaptureOptions : BIOCaptureOptions

- (instancetype __nonnull)init;

- (instancetype __nonnull)initWithMode:(FingerCaptureMode)mode;

@property (nonatomic, assign) FingerCaptureMode bioCaptureMode;
@property (nonatomic, strong, nullable) NSArray<BIOTemplate *> *referenceTemplates;
@property (nonatomic) int threshold;

@end
