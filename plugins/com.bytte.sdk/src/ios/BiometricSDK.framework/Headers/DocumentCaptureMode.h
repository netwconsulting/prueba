//
//  BIOMRZCaptureMode.h
//  BiometricSDK
//
//  Created by WASAG Kamil - EXT-REPERIO (MORPHO) on 24/05/2016.
//  Copyright © 2016 WASAG Kamil - EXT-REPERIO (MORPHO). All rights reserved.
//



/**
 *  @author Morpho
 *
 *  @brief Enum used to configure mode of DocumentCaptureHandler
 */
typedef NS_ENUM(NSInteger, DocumentCaptureMode) {
    /**
     *  @author Morpho
     *
     *  @brief Set DocumentCaptureHandler to read MRZ from document
     */
    DocumentCaptureModeReadMRZ,
    /**
     *  @author Morpho
     *
     *  @brief Set DocumentCaptureHandler to read MRZ lines and retrieve an Image from the document
     */
    DocumentCaptureModeMRZImageMedium,
    /**
     *  @author Morpho
     *
     *  @brief Set DocumentCaptureHandler to read MRZ lines and retrieve an Image from the document
     *  @deprecated Has been replaced by DocumentCaptureModeMRZImage
     */
    DocumentCaptureModeMRZImageHigh __attribute__((deprecated)) = DocumentCaptureModeMRZImageMedium,
    /**
     *  @author Morpho
     *
     *  @brief Set DocumentCaptureHandler to read MRZ lines and retrieve a still Image from the document
     */
    DocumentCaptureModeMRZImageStillMedium,
    /**
     *  @author Morpho
     *
     *  @brief Set DocumentCaptureHandler to read MRZ lines and retrieve a still Image from the document
     *  @deprecated Has been replaced by DocumentCaptureModeMRZImageStill
     */
    DocumentCaptureModeMRZImageStillHigh __attribute__((deprecated)) = DocumentCaptureModeMRZImageStillMedium,
    /**
     *  @author Morpho
     *
     *  @brief Set DocumentCaptureHandler to take image of document
     */
    DocumentCaptureModeDocumentImage,
    /**
     *  @author Morpho
     *
     *  @brief Set DocumentCaptureHandler to read barCode
     */
    DocumentCaptureModeBarcode,
    /**
     *  @author Morpho
     *
     *  @brief Set DocumentCaptureHandler ro read barcode in PDF417 standard
     */
    DocumentCaptureModeBarcodePDF417,
    /**
     *  @author Morpho
     *
     *  @brief Set DocumentCaptureHandler to capture an image at best device's video resolution of ID1,ID2 and ID3 documents
     */
    DocumentCaptureModeVeryLowID,
    /**
     *  @author Morpho
     *
     *  @brief Set DocumentCaptureHandler to capture an image at best device's video resolution of A4 document
     */
    DocumentCaptureModeVeryLowA4,
    /**
     *  @author Morpho
     *
     *  @brief Set DocumentCaptureHandler to capture an image at best device's video resolution of MRZ, ID1,ID2 and ID3 documents
     */
    DocumentCaptureModeMRZMediumVeryLowID,
    /**
     *  @author Morpho
     *
     *  @brief Set DocumentCaptureHandler to capture an image at best device's video resolution of MRZ, A4 documents
     */
    DocumentCaptureModeMRZMediumVeryLowA4
    
};

NSString * _Nonnull NSStringFromDocumentCaptureMode(DocumentCaptureMode captureMode);
