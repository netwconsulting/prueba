//
//  BIOEvent.h
//  BiometricSDK
//
//  Created by RIBEIRO Tiago on 26/07/2017.
//  Copyright © 2017 MORPHO. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <BiometricSDK/BIODiagnosticEvent.h>

@interface BIOEvent : NSObject <BIODiagnosticEvent>

- (instancetype _Nonnull)initWithType:(BIOEventType)type;


@end
