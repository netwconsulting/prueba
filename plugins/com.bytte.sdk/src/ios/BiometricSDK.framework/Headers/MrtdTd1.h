//
//  MrtdTd1.h
//  BiometricSDK
//
//  Created by RIBEIRO Tiago on 03/05/17.
//  Copyright © 2017 MORPHO. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <BiometricSDK/MRZRecord.h>

@interface MrtdTd1 : MRZRecord

@property (nonatomic, strong, nullable) NSString *optional;
@property (nonatomic, strong, nullable) NSString *optional2;

@end
