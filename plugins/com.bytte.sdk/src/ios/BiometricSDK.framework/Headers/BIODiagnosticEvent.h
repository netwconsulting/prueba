//
//  BIODiagnosticEvent.h
//  BiometricSDK
//
//  Created by RIBEIRO Tiago on 19/07/2017.
//  Copyright © 2017 MORPHO. All rights reserved.
//

#import <BiometricSDK/BIOEventType.h>
#import <BiometricSDK/BIOEventParameters.h>
#import <BiometricSDK/BIOEventResult.h>
#import <BiometricSDK/BIOMetadata.h>
#import <BiometricSDK/BIODeviceStatus.h>
#import "BIONSDictionaryAdapter.h"

@protocol BIODiagnosticEvent <BIONSDictionaryAdapter>

@property (nonatomic) BIOEventType type;
@property (nonatomic, strong) NSDate * _Nonnull date;
@property (nonatomic) long duration;
@property (nonatomic) BIOEventParameters * _Nonnull eventParameters;
@property (nonatomic) BIOEventResult * _Nonnull eventResult;
@property (nonatomic) BIOMetadata * _Nonnull metadata;
@property (nonatomic) BIODeviceStatus * _Nonnull deviceStatus;

- (instancetype _Nullable)initWithDictionary:(NSDictionary * _Nonnull)dict withError:(NSError * _Nullable * _Nullable)error;

@end
