//
//  BIOMRZOption.h
//  BiometricSDK
//
//  Created by WASAG Kamil - EXT-REPERIO (MORPHO) on 24/05/2016.
//  Copyright © 2016 WASAG Kamil - EXT-REPERIO (MORPHO). All rights reserved.
//

#import <BiometricSDK/BIOMode.h>
#import <BiometricSDK/DocumentCaptureMode.h>
#import <BiometricSDK/BIOOCR.h>
#import <BiometricSDK/BIORectification.h>

/**
 * Object used to configure document handler. It allow to set: kind of informaiton to take (mode), camera, flash, timeout, buildin overlay, log level
 */
@interface DocumentCaptureOptions : BIOMode

/**
 *  @author Morpho
 *
 *  @brief Default initialiser
 *
 *  @return Instance of object with properties set to predefine values: 
 *      camera = BIOCameraFront;
 *      torch = BIOTorchOFF;
 *      overlay = BIOOverlayON;
 *      captureTimeout = 120;
 *      logLevel = BIOLogLevelNone;
 *      captureMode=DocumentCaptureModeBarcode;
 *      orientation=BIOOrientationLandscapeLeft;
 */
- (instancetype _Nonnull)init;

/**
 *  @author Morpho
 *
 *  @brief Initialiser that set a all properties except mode
 *
 *  @return Instance of object with properties set to predefine values:
 *      camera = BIOCameraFront;
 *      torch = BIOTorchOFF;
 *      overlay = BIOOverlayON;
 *      captureTimeout = 120;
 *      logLevel = BIOLogLevelNone;
 *      orientation=BIOOrientationLandscapeLeft;
 */
- (instancetype _Nonnull)initWithMode:(DocumentCaptureMode)mode;

/**
 *  @author Morpho
 *
 *  @brief Initialiser that set only logLevel and orientation
 *
 *  @return Instance of object with properties set to predefine values:
 *      logLevel = BIOLogLevelNone;
 *      orientation=BIOOrientationAuto;
 */
-(instancetype _Nonnull)initWithMode:(DocumentCaptureMode)mode
                 withCamera:(BIOCamera)camera
                 witchTorch:(BIOTorch)torch
                withOverlay:(BIOOverlay)overlay
                withTimeout:(NSTimeInterval)timeout;

@property (nonatomic, assign) DocumentCaptureMode captureMode;
@property (nonatomic, assign) BIOOCR OCREnable;
@property (nonatomic, assign) BIORectification rectification;

@end
