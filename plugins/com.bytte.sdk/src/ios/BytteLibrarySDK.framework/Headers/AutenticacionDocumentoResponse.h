//
//  AutenticacionDocumentoResponse.h
//  BytteLibrarySDK
//
//  Created by Allan Diaz on 10/17/17.
//  Copyright © 2017 Allan Diaz. All rights reserved.
//

#import "BaseResponse.h"
#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface AutenticacionDocumentoResponse : BaseResponse
@property (nonatomic) NSNumber * ScoreDactilar;
@property (nonatomic) NSNumber * ScoreDactilarValor;
@property (nonatomic) NSNumber * ScoreANIOCR;
@property (nonatomic) NSNumber * ScoreANIBarCode;
@property (nonatomic) NSNumber * ScoreOCRBarCode;
@property (nonatomic) NSNumber * ScoreFace;
@property (nonatomic) NSNumber * ScoreFaceValor;
@property (nonatomic) NSNumber * ScoreDocumento;
@property (nonatomic) NSNumber * StatusDactilar;
@property (nonatomic) NSNumber * StatusANIOCR;
@property (nonatomic) NSNumber * StatusANIBarCode;
@property (nonatomic) NSNumber * StatusOCRBarCode;
@property (nonatomic) NSNumber * StatusFace;
@property (nonatomic) NSNumber * StatusDocumento;
@property (nonatomic) bool AutenticadoDocumento;

//Convierte a JSON
-(NSString *) Convert2JSONValue;
@end
