//
//  BIOMRZCaptureHandlerDelegate.h
//  BiometricSDK
//
//  Created by WASAG Kamil - EXT-REPERIO (MORPHO) on 24/05/2016.
//  Copyright © 2016 WASAG Kamil - EXT-REPERIO (MORPHO). All rights reserved.
//

#import <Foundation/Foundation.h>
#import <BiometricSDK/DocumentMRZLine.h>
#import <BiometricSDK/DocumentImage.h>
#import <BiometricSDK/BIODocInfo.h>
#import <BiometricSDK/BIODocumentTrackingInformation.h>
#import <BiometricSDK/BIOReplayProtocol.h>
#import <BiometricSDK/MRZRecord.h>

/**
 *  @author Morpho
 *
 *  @brief Protol descorbe mathods of DocumentCaptureHandler
 */
@protocol DocumentCaptureHandlerDelegate <BIOReplayProtocol>

@optional

/**
 *  @author Morpho
 *
 *  @brief Method call when capture handler read MRZ from document.
 *
 *  @param lines Array of readed lines.
 *  @param error NSError if any occured.
 */

- (void)documentHandlerReadMRZ:(NSArray<DocumentMRZLine*>* _Nonnull)lines record:(MRZRecord * __nonnull)record withError:(NSError* _Nullable)error;

///**
// *  @author Morpho
// *
// *  @brief Method call when capture handler capture image of document.
// *
// *  @deprecated Method will never be called. Use `documentHandlerCapturedDocumentImage:withError:`
// *
// *  @param images Array of captured images.
// *  @param error NSError if any occured.
// */
//- (void)documentHandlerReadDocumentImage:(NSArray<DocumentImage*>* _Nonnull)images withError:(NSError* _Nullable)error __deprecated_msg("Method will never be called. Use `documentHandlerCapturedDocumentImage:withError:`");


/**
 @author Morpho
 
 @brief Method called when capture handler has captured the image of the document.

 @param documentImage Object that contains the image of the document.
 */
- (void)documentHandlerCapturedDocumentImage:(DocumentImage * _Nonnull)documentImage;


/**
 @author Morpho
 
 @brief Method called when capture handler has captured a rectification image.
 
 @warning Only available when rectification is enabled. Can be called multiple times.

 @param rectificationImage Object that contains the rectificattion image.
 @param label String the describes the location of the returned image in the whole document.
 */
- (void)documentHandlerCapturedDocumentRectificationImage:(DocumentImage * _Nonnull)rectificationImage withLabel:(NSString * _Nonnull)label;


/**
 *  @author Morpho
 *
 *  @brief Method called when barcode is read.
 *
 *  @param barcodeInformation Information from barcode given as array of strings.
 *  @param error               Error if any occured if not nil insted.
 */
- (void)documentHandlerReadBarCode:(NSArray<NSString*>* _Nullable)barcodeInformation withError:(NSError* _Nullable)error;

/**
 *  @author Morpho
 *
 *  @brief Callback invoked when document handler return info about document.
 *
 *  @param info  Information about document (to blur, shake etc.)
 *  @param error error with decription of problem.
 */
- (void)documentHandlerReceiveInfo:(BIODocInfo)info withError:(NSError* _Nullable)error;

/**
 *  @author Morpho
 *
 *  @brief Callback to retrieve the tracking position of documents.
 *
 *  @param trackingInfo Array of object thats provide tracking information about scanning document.
 *  @param error        Error if any occurred, nil if everything is correct.
 */
- (void)documentHandlerGetTrackingInformation:(NSArray<BIODocumentTrackingInformation*>* _Nonnull)trackingInfo withError:(NSError* _Nullable)error;


@end
