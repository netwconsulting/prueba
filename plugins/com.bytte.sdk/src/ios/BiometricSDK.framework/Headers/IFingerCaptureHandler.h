//
//  IFingerprintCaptureHandler.h
//  BiometricSDK
//
//  Created by RIBEIRO Tiago on 08/03/17.
//  Copyright © 2017 MORPHO. All rights reserved.
//

#import <BiometricSDK/FingerCaptureHandlerDelegate.h>
#import <BiometricSDK/FingerCaptureOptions.h>
#import <BiometricSDK/BIOCaptureHandlerError.h>
#import <BiometricSDK/IBIOCaptureHandler.h>

@protocol FingerCaptureHandler <BIOCaptureHandler>

@property (nonatomic, weak) id<FingerCaptureHandlerDelegate> _Nullable delegate;

@end
