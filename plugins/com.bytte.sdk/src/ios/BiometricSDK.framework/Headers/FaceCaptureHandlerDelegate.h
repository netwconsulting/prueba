//
//  BCBioCaptureHandlerDelegate.h
//  BiometricSDK
//
//  Created by WASAG Kamil - EXT-REPERIO (MORPHO) on 06/04/2016.
//  Copyright © 2016 WASAG Kamil - EXT-REPERIO (MORPHO). All rights reserved.
//

#import <BiometricSDK/BIOImage.h>
#import <BiometricSDK/BIOTrackingInfo.h>
#import <BiometricSDK/BIOCapturingError.h>
#import <BiometricSDK/BIOCapturingInfo.h>
#import <BiometricSDK/BIOReplayProtocol.h>
#import <BiometricSDK/BIOChallengeInfo.h>

/**
 * Delegation object of BCBioCaptureHandler needs to implement BCBioCaptureHandlerDelegate protocol.
 * It will inform delegate object about capture process, message for user, and bio information tracking information.
 * All methods in this protocol are optional.
 */

@protocol FaceCaptureHandlerDelegate <BIOReplayProtocol>

@optional

/**
 * Method that is triggered when BCBioCaptureHandler find biometric data face/iris/fingerprints and finish handle with data.
 *
 * @warning Capturing will stop before calling this method.
 *
 * @param images array of images that FaceCaptureHandler find in image from camera.
 * @param biometrics object that describe what biometrics data was captured e.g. left iris.
 * @param error error that inform if something was wrong with captured image.
 */
- (void)captureFinishedWithImages:(NSArray<BIOImage* >* _Nullable)images withBiometrics:(BIOBiometrics* _Nullable)biometrics withError:(NSError* _Nullable)error;

/**
 * Method that is triggered when BCBioCaptureHandler receive any info for user for user e.g. to back to camera field.
 *
 * @deprecated This mehtod will no longer be called. Use receiveBioCaptureInfo:withChallengeInfo:withError:
 *
 * @param info Parameter that describe action from user that needs to be done to finish capturing.
 * @param error error if something was wrong otherwise nil.
 */
- (void)receiveBioCaptureInfo:(BIOCapturingInfo)info withError:(NSError* _Nullable)error __deprecated_msg("Use receiveBioCaptureInfo:withChallengeInfo:withError:");


/**
 * Method that is triggered when BCBioCaptureHandler receive any info for user for user e.g. to back to camera field.
 *
 * @param info Parameter that describe action from user that needs to be done to finish capturing.
 * @param challengeInfo information about the challenges (current challenge number and total number of challenges).
 * @param error error if something was wrong otherwise nil.
 */
- (void)receiveBioCaptureInfo:(BIOCapturingInfo)info withChallengeInfo:(BIOChallengeInfo * _Nullable)challengeInfo withError:(NSError* _Nullable)error;


/**
 * Method that is triggered when BCBioCaptureHandler receive any info for user for user e.g. to back to camera field.
 *
 * @param info Parameter that describe action from user that needs to be done to finish capturing.
 */
- (void)receiveBioCaptureInfoOnTimeOut:(BIOCapturingInfo)info  withError:(NSError* _Nullable)error __deprecated_msg("Replaced by `- (void)captureIsLockedFor:(NSInteger)seconds`");

///**
// * Method that is triggered when BCBioCaptureHandler receive any info for user for user e.g. to back to camera field.
// *
// * @deprecated This method will no longer be called.
// *
// * @param info Parameter that describe action from user that needs to be done to finish capturing.
// */
//- (void)receiveBioCaptureInfoOnUnsuccesfulAttempt:(BIOCapturingInfo)info  withError:(NSError* _Nullable)error __deprecated_msg("This method will no longer be called.");
//

///**
// * Method that porvides information where are curently bio information on screen e.g. face bounds;
// *
// * @deprecated This method will no longer be called.
// *
// * @param bioTrackingInfo array that contains information where on picture are currenlty bio information and which.
// * @param error error if any occurred or nil.
// */
//- (void)trackedInfo:(NSArray<BIOTrackingInfo*>* _Nullable)bioTrackingInfo withError:(NSError* _Nullable)error __deprecated_msg("Thismethod will no longer be called.");
//


/**
 *
 * Method that notifies when the capture is locked and for how many secnods
 *
 * @param seconds number of seconds remaining until capture is unlocked
 */
- (void)captureIsLockedFor:(NSInteger)seconds;

@end
