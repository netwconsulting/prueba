//
//  BIOEventType.h
//  BiometricSDK
//
//  Created by RIBEIRO Tiago on 26/07/2017.
//  Copyright © 2017 MORPHO. All rights reserved.
//

#ifndef BIOEventType_h
#define BIOEventType_h

typedef NS_ENUM(NSUInteger, BIOEventType) {
    BIOEventTypeLicenseManagerHandlerCreation,
    BIOEventTypeLicenseManagerHandlerDestroy,
    BIOEventTypeLicenseCheck,
    BIOEventTypeLicenseCreation,
    BIOEventTypeLicenseRemove,
    BIOEventTypeLicenseActivate,
    BIOEventTypeBIOCaptureHandlerCreation,
    BIOEventTypeBIOCaptureHandlerDestroy,
    BIOEventTypeBIOMatcherHandlerCreation,
    BIOEventTypeBIOMatcherHandlerDestroy,
    BIOEventTypeDocumentCaptureHandlerCreation,
    BIOEventTypeDocumentCaptureHandlerDestroy,
    BIOEventTypeStartCapture,
    BIOEventTypeStopCapture,
    BIOEventTypeStartPreview,
    BIOEventTypeStopPreview,
    BIOEventTypeBiometricCapture,
    BIOEventTypeBiometricDetection,
    BIOEventTypeInfoFeedback,
    BIOEventTypeAuthentication,
    BIOEventTypeIdentification,
    BIOEventTypeDocumentImageCapture,
    BIOEventTypeDocumentFieldImageCapture,
    BIOEventTypeDocumentMRZCapture,
    BIOEventTypeDocumentQRCodeCapture,
    BIOEventTypeDocumentInfoFeedback,
    BIOEventTypeForceCapture
};

NSString * _Nonnull NSStringFromBIOEventType(BIOEventType type);
BIOEventType BIOEventTypeFromNSString(NSString * _Nonnull str, NSError * _Nullable * _Nullable error);


#endif /* BIOEventType_h */
