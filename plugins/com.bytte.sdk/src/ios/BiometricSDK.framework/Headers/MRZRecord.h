//
//  MRZRecord.h
//  BiometricSDK
//
//  Created by RIBEIRO Tiago on 28/04/17.
//  Copyright © 2017 MORPHO. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <BiometricSDK/MRZStringConvertible.h>
#import <BiometricSDK/MRZDocumentCode.h>
#import <BiometricSDK/MRZDate.h>
#import <BiometricSDK/MRZSex.h>
#import <BiometricSDK/MRZFormat.h>

@interface MRZRecord : NSObject <MRZStringConvertible>

@property (nonatomic, assign) MRZDocumentCode code;
@property (nonatomic, assign) char code1;
@property (nonatomic, assign) char code2;
@property (nonatomic, strong, nullable) NSString *issuingCountry;
@property (nonatomic, strong, nullable) NSString *documentNumber;
@property (nonatomic, strong, nullable) NSString *surname;
@property (nonatomic, strong, nullable) NSString *givenNames;
@property (nonatomic, strong, nullable) MRZDate *dateOfBirth;
@property (nonatomic, assign) MRZSex sex;
@property (nonatomic, strong, nullable) MRZDate *expirationDate;
@property (nonatomic, strong, nullable) NSString *nationality;
@property (nonatomic, strong, nullable) MRZFormat *format;


- (instancetype __nonnull)initWithMRZFormat:(MRZFormat * __nonnull)mrzFormat;

@end
