//
//  BIOEnvironment.h
//  BiometricSDK
//
//  Created by RIBEIRO Tiago on 28/07/2017.
//  Copyright © 2017 MORPHO. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BIONSDictionaryAdapter.h"

@interface BIOEnvironment : NSObject <BIONSDictionaryAdapter>

@end
