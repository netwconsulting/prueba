//
//  BIOCaptureMode.h
//  BiometricSDK
//
//  Created by WASAG Kamil - EXT-REPERIO (MORPHO) on 04/04/2016.
//  Copyright © 2016 WASAG Kamil - EXT-REPERIO (MORPHO). All rights reserved.
//

/**
 *
 *  Determine in what mode FaceCaptureMode will work
 */
typedef NS_ENUM(NSUInteger,FaceCaptureMode){
    /**
     *   Sets FaceCaptureHandler to very low challange of liveness
     */
    FaceCaptureModeChallengeVeryLow,
    /**
     *   Sets FaceCaptureHandler to low challange of liveness
     */
    FaceCaptureModeChallengeLow,
    /**
     *   Sets FaceCaptureHandler to medium challange of liveness
     */
    FaceCaptureModeChallengeMedium,
    /**
     *   Sets FaceCaptureHandler to high challange of liveness
     */
    FaceCaptureModeChallengeHigh,
    /**
     *   Sets FaceCaptureHandler to very high challange of liveness
     */
    FaceCaptureModeChallengeVeryHigh,
        
    /**
     *   Sets FaceCaptureHandler to low level liveness detection
     */
    FaceCaptureModeLivenessLow,
    
    /**
     *   Sets FaceCaptureHandler to medium level liveness detection
     */
    FaceCaptureModeLivenessMedium,
    
    /**
     *   Sets FaceCaptureHandler to high level liveness detection
     */
    FaceCaptureModeLivenessHigh,
    
    /**
     *   Sets FaceCaptureHandler to track face and get image
     */
    FaceCaptureModeTrackDefault,
} ;


NSString * NSStringFromFaceCaptureMode(FaceCaptureMode faceCaptureMode);


