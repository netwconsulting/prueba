//
//  CameraViewController.h
//  BytteLibrarySDK
//
//  Created by Allan Diaz on 12/19/16.
//  Copyright © 2016 Allan Diaz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import "InfoDocumento.h"

@protocol CapturaBarCodeDelegate <NSObject>
@optional
- (void)doCapturaBarCode:(InfoDocumento *)infoDocumento
            withPassword:(NSString *)filePassword;
@end
@interface CameraViewController : UIViewController
@property short      timeOutController;
//Init
- (id)initWithKey:(NSString *)Key AndUIImageGuide:(UIImage *) imgGuia WithDet:(UIImage *) imDetalle;
- (void) setFilePassword:(NSString *) password;
- (void)releaseObjects;

//Referencia al Delegado
@property (strong, nonatomic)  id <CapturaBarCodeDelegate> capturaBarCodeDelegate;
@end
