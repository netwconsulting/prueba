//
//  BIORectification.h
//  BiometricSDK
//
//  Created by WASAG Kamil - EXT-REPERIO (MORPHO) on 14/12/2016.
//  Copyright © 2016 MORPHO. All rights reserved.
//

#ifndef BIORectification_h
#define BIORectification_h

typedef NS_ENUM(int32_t, BIORectification){
    BIORectificationDisabled = 0,
    BIORectificationEnabled = 1
};

#endif /* BIORectification_h */
