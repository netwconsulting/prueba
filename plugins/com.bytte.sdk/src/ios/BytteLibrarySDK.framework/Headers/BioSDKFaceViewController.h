//
//  BioSDKFaceViewController.h
//  BytteLibrarySDK
//
//  Created by Allan Diaz on 7/28/17.
//  Copyright © 2017 Allan Diaz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import "FaceResponse.h"


@protocol BioSDKFaceDelegate <NSObject>
@optional
- (void)doBioSDKFaceDelegate:(FaceResponse *)faceResponse
                withPassword:(NSString *)filePassword;
@end
@interface BioSDKFaceViewController : UIViewController
@property short      timeOutController;
@property short      faceCaptureMode;
-(void) setFilePassword:(NSString *) password;

//Referencia al Delegado
@property (strong, nonatomic)  id <BioSDKFaceDelegate> bioSDKFaceDelegate;
@end
