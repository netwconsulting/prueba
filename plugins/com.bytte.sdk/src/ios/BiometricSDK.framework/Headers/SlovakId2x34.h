//
//  SlovakId2x34.h
//  BiometricSDK
//
//  Created by RIBEIRO Tiago on 03/05/17.
//  Copyright © 2017 MORPHO. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <BiometricSDK/MRZRecord.h>

@interface SlovakId2x34 : MRZRecord

@property (nonatomic, strong, nullable) NSString *optional;

@end
