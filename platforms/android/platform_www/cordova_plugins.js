cordova.define('cordova/plugin_list', function(require, exports, module) {
module.exports = [
  {
    "id": "com.bytte.sdk.BytteBioLib",
    "file": "plugins/com.bytte.sdk/www/BytteBioLib.js",
    "pluginId": "com.bytte.sdk",
    "clobbers": [
      "byttebiolib"
    ]
  },
  {
    "id": "com.bytte.sdk.BytteBioLibChile",
    "file": "plugins/com.bytte.sdk/www/BytteBioLibChile.js",
    "pluginId": "com.bytte.sdk",
    "clobbers": [
      "byttebiolibchile"
    ]
  }
];
module.exports.metadata = 
// TOP OF METADATA
{
  "cordova-plugin-whitelist": "1.3.3",
  "com.bytte.sdk": "3.6.3"
};
// BOTTOM OF METADATA
});