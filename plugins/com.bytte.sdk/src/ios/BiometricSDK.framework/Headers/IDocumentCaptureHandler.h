//
//  IBIOMRZCaptureHandler.h
//  BiometricSDK
//
//  Created by WASAG Kamil - EXT-REPERIO (MORPHO) on 23/05/2016.
//  Copyright © 2016 WASAG Kamil - EXT-REPERIO (MORPHO). All rights reserved.
//


#import <BiometricSDK/IBIOGenericCaptureHandler.h>
#import <BiometricSDK/DocumentCaptureHandlerDelegate.h>
#import <BiometricSDK/BIOOCR.h>
#import <BiometricSDK/BIORectification.h>
/**
 *  @author Morpho
 *
 *  @brief Protocol with public api of handler responsible for capturing information from documents
 */
@protocol DocumentCaptureHandler <BIOGenericCaptureHandler>


/**
 *  @author Morpho
 *
 *  @brief Delegate object that will be infromed about captured information.
 */
@property (nonatomic, weak) id<DocumentCaptureHandlerDelegate> _Nullable delegate;

/**
 *  @author Morpho
 *
 *  @brief Object with set of information of which mode handler works e.g.: which camera is selected, if torch is enable or not....
 */
@property (nonatomic, strong, readonly) DocumentCaptureOptions* _Nullable mode;

/**
 *  @author Morpho
 *
 *  @brief Method that force document handler to capture document picture
 *
 *  @return error if any occured.
 */
- (NSError* _Nullable)forceCapture;

/**
 *  @author Morpho
 *
 *  @brief Method that Disable/Enable OCR reading from document handler
 *
 *  @param ocr   Parameter that sets OCR reading
 *  @param error object with error description if any occured otherwise nil
 */
- (void)setOCR:(BIOOCR)ocr withError:(NSError  *_Nullable __autoreleasing *_Nullable)error;

/**
 *  @author Morpho
 *
 *  @brief Method that Disable/Enable rectification.
 *
 *  @param rectification parameter that sets rectification to Disable/Enable
 *  @param error         object with error description if any occured otherwise nil
 */
- (void)setRectification:(BIORectification)rectification withError:(NSError *_Nullable __autoreleasing *_Nullable)error;

@end
