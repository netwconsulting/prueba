//
//  BCBioTraking.h
//  BiometricSDK
//
//  Created by WASAG Kamil - EXT-REPERIO (MORPHO) on 06/04/2016.
//  Copyright © 2016 WASAG Kamil - EXT-REPERIO (MORPHO). All rights reserved.
//

#import <BiometricSDK/BIOBiometrics.h>
#import <UIKit/UIKit.h>
/**
 *  A BCBioTraking is used to storage data about taken biometrics frame, location and modality
 */
@interface BIOTrackingInfo : BIOBiometrics

/**
 * Default initalizer that setups biometricLocation for BCBiometricLocationFaceFrontal and biometricModality BCBiometricModalityFace and frame to CGRectZero
 *
 * @return Instance of BCBioTraking with preseted properties
 */
- (instancetype _Nonnull)init;

/**
 *  Initializer that allow to set all parameters
 *
 *  @param biometricLocation location to set
 *  @param biometricModality modality to set
 *  @param frame frame of biometrics
 *
 *  @return Instance of BCBioTraking with parameters set
 */
- (instancetype _Nonnull)initWithBiometricLocation:(BIOLocation)biometricLocation
                    withBiometricModality:(BIOModality)biometricModality
                            withFrame:(CGRect)frame;

/**
 *  Class method that creates instance with parametres set
 *
 *  @param biometricLocation location to set
 *  @param biometricModality modality to set
 *  @param frame frame of biometrics
 *
 *  @return Instance of BCBioTraking with parameters set
 */
+ (instancetype _Nonnull)bioTrakingWithBiometricLocation:(BIOLocation)biometricLocation
                          withBiometricModality:(BIOModality)biometricModality
                                  withFrame:(CGRect)frame;

/**
 *   Property stores frame of taken biometrics
 */
@property (nonatomic) CGRect frame;

@end
