//
//  IBIOCaptureHandler.h
//  BiometricSDK
//
//  Created by WASAG Kamil - EXT-REPERIO (MORPHO) on 27/04/2016.
//  Copyright © 2016 WASAG Kamil - EXT-REPERIO (MORPHO). All rights reserved.
//

#import <BiometricSDK/BIOCaptureOptions.h>
#import <BiometricSDK/BIOCaptureHandlerError.h>
#import <BiometricSDK/IBIOGenericCaptureHandler.h>
/**
 *  @brief A BCBioCaptureHandler is class that prowides all mechanism to handle capturing bio informations Face/Iris/Fingerprints.
 *
 * After init object first you should set preview and delegate properties, and run captureWithOptions:withCompletionHandler: to setup capturing.
 *
 * All operations are calle in separated thread.
 *
 * Next start preview and capture. Methods start and stop capture or preview must be run in main thread.
 *
 */

@protocol BIOCaptureHandler <BIOGenericCaptureHandler>

///**
// *  Delegation object that will receive information from BCBioCaptureHandler about state of capturing.
// *  It must implement BCBioCaptureHandlerDelegate protocol.
// */
//@property (nonatomic, weak) id<BIOCaptureHandlerDelegate> _Nullable delegate;

/**
 *  @brief Options of current capturing session
 */
@property (nonatomic, strong, readonly) BIOCaptureOptions* _Nonnull mode;


@end
