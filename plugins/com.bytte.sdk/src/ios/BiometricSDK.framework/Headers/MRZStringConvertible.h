//
//  MRZStringConvertible.h
//  BiometricSDK
//
//  Created by RIBEIRO Tiago on 28/04/17.
//  Copyright © 2017 MORPHO. All rights reserved.
//

@protocol MRZStringConvertible <NSObject>

- (instancetype __nonnull)init;

- (NSError * __nullable)fromMRZString:(NSString * __nonnull)mrzString;

@end
