//
//  BIOMatchingCandidate.h
//  BiometricSDK
//
//  Created by WASAG Kamil - EXT-REPERIO (MORPHO) on 03/05/2016.
//  Copyright © 2016 WASAG Kamil - EXT-REPERIO (MORPHO). All rights reserved.
//

#import <Foundation/Foundation.h>


/**
 *  @author Morpho
 *
 *  @brief Object contain information about potential candiate of matching process
 */
@interface BIOMatchingCandidate : NSObject

/**
 *  @author Morpho
 *
 *  @brief UUID of user used for matching
 */
@property (nonatomic,strong,nonnull) NSString *UUID;
/**
 *  @author Morpho
 *
 *  @brief Score of matching process range 
 */
@property (nonatomic) long score;
@end
