//
//  Util.h
//  MBiometric
//
//  Created by Allan Diaz on 11/2/16.
//  Copyright © 2016 Allan Diaz. All rights reserved.
//
#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>
#import <UIKit/UIKit.h>
#import "InfoDocumento.h"
#import "DocumentoInfoResponse.h"


/*Definiciones para Cedula */
#define DOC_VERSIONCEDULAPOS   0x00
#define DOC_VERSIONCEDULALEN   0x02

#define DOC_NUMEROTARJETAPOS   0x02
#define DOC_NUMEROTARJETALEN   0x0C
#define DOC_NUMEROCEDULAPOS    0x30
#define DOC_NUMEROCEDULALEN    0x0A
#define DOC_PRIMERAPELLIDOPOS  0x3A
#define DOC_PRIMERAPELLIDOLEN  0x17
#define DOC_SEGUNDOAPELLIDOPOS 0x51
#define DOC_SEGUNDOAPELLIDOLEN 0x17
#define DOC_PRIMERNOMBREPOS    0x68
#define DOC_PRIMERNOMBRELEN    0x17
#define DOC_SEGUNDONOMBREPOS   0x7F
#define DOC_SEGUNDONOMBRELEN   0x17
#define DOC_ESTADOCIVILPOS     0x96
#define DOC_ESTADOCIVILLEN     0x01
#define DOC_SEXOPOS            0x97
#define DOC_SEXOLEN            0x01
#define DOC_FECHANACIMIENTOPOS 0x98
#define DOC_FECHANACIMIENTOLEN 0x08
#define DOC_LUGARNACIMIENTOPOS 0xA0
#define DOC_LUGARNACIMIENTOLEN 0x06
#define DOC_RHPOS              0xA6
#define DOC_RHLEN              0x03
#define DOC_TIPODEDO1POS       0xA9
#define DOC_TIPODEDO1LEN       0x01
#define DOC_TIPODEDO2POS       0x014A
#define DOC_TIPODEDO2LEN       0x01

//Definiciones para tarjeta de Identidad
#define TI_NUMEROTARJETAPOS    0x02
#define TI_NUMEROTARJETALEN    0x0C
#define TI_NUMEROCEDULAPOS     0x30
#define TI_NUMEROCEDULALEN     0x0B
#define TI_PRIMERAPELLIDOPOS   0x3B
#define TI_PRIMERAPELLIDOLEN   0x17
#define TI_SEGUNDOAPELLIDOPOS  0x52
#define TI_SEGUNDOAPELLIDOLEN  0x17
#define TI_PRIMERNOMBREPOS     0x69
#define TI_PRIMERNOMBRELEN     0x17
#define TI_SEGUNDONOMBREPOS    0x80
#define TI_SEGUNDONOMBRELEN    0x17
#define TI_ESTADOCIVILPOS      0x97
#define TI_ESTADOCIVILLEN      0x01
#define TI_SEXOPOS             0x98
#define TI_SEXOLEN             0x01
#define TI_FECHANACIMIENTOPOS  0x99
#define TI_FECHANACIMIENTOLEN  0x08
#define TI_LUGARNACIMIENTOPOS  0xA1
#define TI_LUGARNACIMIENTOLEN  0x06
#define TI_RHPOS               0xA7
#define TI_RHLEN               0x03
#define TI_TIPODEDO1POS        0xAA
#define TI_TIPODEDO1LEN        0x01
#define TI_TEMPLATE1POS        0xAB
#define TI_TEMPLATE1LEN        0xA0
#define TI_TIPODEDO2POS        0x014B
#define TI_TIPODEDO2LEN        0x01
#define TI_TEMPLATE2POS        0x014C
#define TI_TEMPLATE2LEN        0xA0

#define STRING_EMPTY             @"";
#define TIME_OUT_DEFAULT       20

extern NSString * const INTERNAL_ZIPP;
@interface Util : NSObject

//Estaticos
+(void) changeBackground:(UIView *) sView;
+(NSString *)trimString:(NSString *)stringData;
+(NSString *)decodeStringWithUTF8String:(NSData *)binData withRangePos:(int)pos andLen:(int)len;
+(NSString *)decodeStringNonLossyASCIIStringEncoding:(NSData *)binData withRangePos:(int)pos andLen:(int)len;
+(NSString *)convertFechaNacimiento:(NSString *)fechaDocumento;
+(UIImage  *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize;
+(UIImage  *)cropImage:(UIImage *)imageToCrop toRect:(CGRect)rect;
+(NSString *)generaJSONRequestServer:(NSString *)procesoId
                      withNumHuellas:(NSString *)numHuellas
                    withFingerNumber:(NSString *)fingerNumber
                         withCaptura:(NSString *)idCaptura;
+(NSString *)stringFromHexString:(NSString *)str;
+(InfoDocumento *)Convert2InfoDocumento:(NSData *) barCodeInfo;
+(UIImage *) imageFromSampleBuffer:(CMSampleBufferRef) sampleBuffer;
+(CGImageRef) CGImageRefFromSampleBuffer:(CMSampleBufferRef) sampleBuffer;
+(UIImage *) UIImageFromSampleBuffer:(CMSampleBufferRef) sampleBuffer;
+ (UIImage *) UIImageFromSampleBufferFrame:(CMSampleBufferRef) frame;

+(DocumentoInfoResponse *)convertFromStringJSON:(NSString *)sJsonMsg;
+(UIImage *)imageRotatedByDegrees:(UIImage*)oldImage deg:(CGFloat)degrees;
+(UIImage *)grayscaleImage:(UIImage *)image;
+(UIImage *)mirrorHorizontal:(UIImage *)image;

//Lee un archivo desde un path fisico
+(NSData  *)readFileFromPath:(NSString *) fileURL;

//EscribeArchivoLocal (Sin Clave)
+(NSString *)escribeArchivoLocal:(NSData *)infoArchivo;

//EscribeArchivoLocal (Con Clave)
+(NSString *)escribeArchivoLocal:(NSData *)infoArchivo
                    withPassword:(NSString *)filePassword;

//Escribe un archivo con extension (Con Clave)
+(NSString *)escribeArchivoLocalEx:(NSData *)infoArchivo
                     withExtension:(NSString *)extension
                      withPassword:(NSString *)filePassword;

//Escribe un archivo con extension (Sin Clave)
+(NSString *)escribeArchivoLocalEx:(NSData *)infoArchivo
                     withExtension:(NSString *)extension;

+(NSData   *) ConvertHexString2NSData:(NSString *)hex;
+(NSString *) ConvertNSData2HexString:(NSData*)data;

//Retorna si un NSString es Null O Empty
+(BOOL)stringIsNilOrEmpty:(NSString*)aString;
@end
