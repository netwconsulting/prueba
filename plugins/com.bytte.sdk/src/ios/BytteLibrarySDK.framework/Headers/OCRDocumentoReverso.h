//
//  OCRDocumentoReverso.h
//  BytteLibrarySDK
//
//  Created by Allan Diaz on 8/29/17.
//  Copyright © 2017 Allan Diaz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>

@interface OCRDocumentoReverso : NSObject
@property (nonatomic) NSString * FechaNacimiento;
@property (nonatomic) NSString * CiudadExpedicion;
@property (nonatomic) NSString * FechaExpedicion;
@property (nonatomic) NSString * Departamento;
@property (nonatomic) NSString * Ciudad;
@property (nonatomic) NSString * Estatura;
@property (nonatomic) NSString * NumeroDocumento;
@property (nonatomic) NSString * Sexo;
@end
