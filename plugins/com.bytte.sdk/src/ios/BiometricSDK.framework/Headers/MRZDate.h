//
//  MRZDate.h
//  BiometricSDK
//
//  Created by RIBEIRO Tiago on 28/04/17.
//  Copyright © 2017 MORPHO. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MRZDate : NSObject

@property (nonatomic, assign, readonly) int year;
@property (nonatomic, assign, readonly) int month;
@property (nonatomic, assign, readonly) int day;

+ (instancetype __nullable)createMRZDateWithYear:(int)year month:(int)month andDay:(int)day withError:(NSError * __nullable *__nullable)error;

@end
