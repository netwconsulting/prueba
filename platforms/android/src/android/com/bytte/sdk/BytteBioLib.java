/*
 BytteBioLib - Java - Android
 Allan Diaz :) 
 Diciembre de 2016

 Script con las funciones requeridas para implementar el 
 SDK de Bytte Biometrico y Captura codigo de barras desde
 dispositivos Android
 Bytte 2016
*/
package android.com.bytte.sdk;

import org.apache.cordova.CordovaInterface;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaWebView;
import org.apache.cordova.PluginResult;
import org.json.JSONArray;
import org.json.JSONException;


import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.widget.Toast;

import com.microblink.activity.BaseScanActivity;
import com.microblink.util.Log;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import bytte.biometric.com.byttelibrary.e.b;
import bytte.biometric.com.byttelibrary.e.f;

import bytte.biometric.com.byttelibrary.objetosmp.mp_face;
import bytte.biometric.com.byttelibrary.objetosmp.mp_huellas;
import bytte.biometric.com.byttelibrary.ui.cameraDoc;
import bytte.biometric.com.byttelibrary.ui.DetectorActivity;
import bytte.biometric.com.byttelibrary.ui.morpho.face.FmCaptureActivity;
import bytte.biometric.com.byttelibrary.ui.morpho.huella.FbCaptureActivity;
import bytte.biometric.com.byttelibrary.ui.morpho.license.LicenseActivity;


//Clase Modulo Plugin BytteBioLib
public class BytteBioLib extends CordovaPlugin {
    //Callback ContextId para retorno
    private CallbackContext localCallbackContext = null;
    private static final int MY_REQUEST_CODE = 1337;
    private static final int MY_REQUEST_CODE_BIOMETRIC = 1338;
    private static final int MY_REQUEST_CODE_FRONT = 1339;
    private static final int PERMISSION_REQUEST_CODE = 0x123;
    private static final int MY_REQUEST_CODE_LICENSE = 1340;
    private static final int MY_REQUEST_CODE_BIOMETRIC_FACE = 1341;
    private Context context;
    private static int proceso = 0;
    private mp_huellas consumo;
    private mp_face consumoface;

    @Override
    public void initialize(CordovaInterface cordova, CordovaWebView webView) {
        super.initialize(cordova, webView);
        context = cordova.getActivity();
    }

    @Override
    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {
        PluginResult r = new PluginResult(PluginResult.Status.NO_RESULT);
        r.setKeepCallback(true);
        callbackContext.sendPluginResult(r);
        //Status
        boolean bStatus = false;
        //Asigno el CallBack para poder retornar!!
        this.localCallbackContext = callbackContext;

        /*File as = new File(this.cordova.getActivity().getFilesDir(), "Documents");
        if (as.isDirectory()) {
            String[] children = as.list();
            for (int i = 0; i < children.length; i++) {
                new File(as, children[i]).delete();
            }
        }*/

//verifica permisos para el uso de la camara  y el almacenamiento
        List<String> requiredPermissions = new ArrayList<String>();
        if (ContextCompat.checkSelfPermission(this.cordova.getActivity(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            requiredPermissions.add(Manifest.permission.CAMERA);
        }
        if (ContextCompat.checkSelfPermission(this.cordova.getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            requiredPermissions.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
        if (requiredPermissions.size() > 0) {
            String[] permArray = new String[requiredPermissions.size()];
            permArray = requiredPermissions.toArray(permArray);
            ActivityCompat.requestPermissions(this.cordova.getActivity(), permArray, PERMISSION_REQUEST_CODE);
        }

        //Ejecuta la accion Echo!
        if ("echo".equals(action)) {
            //Envia el Argumento 0
            //Envio el CallBack
            echo(args.getString(0));
            bStatus = true;
        }
        //Si llega el mensaje de Inicio de Codigo de barras
        if ("startBarCode".equals(action)) {
            //Argumento 0 es el ID de licencia de Codigo de barras
            //startBarCode(args.getString(0));
            //Argumento 1 es el Password para Ofuscar! Si va Blanco, no Ofusca
            Intent intent = new Intent(this.cordova.getActivity().getApplicationContext(), cameraDoc.class);
            intent.putExtra(BaseScanActivity.EXTRAS_LICENSE_KEY, args.getString(0));
            intent.putExtra("EXTRAS_LICENSEE", args.getString(1).equals("") ? null : args.getString(1));
            intent.putExtra("EXTRAS_TIMEOUT", args.getString(2));
            this.cordova.startActivityForResult((CordovaPlugin) this, intent, MY_REQUEST_CODE);
            bStatus = true;
        }
        //Si llega el mensaje de Inicio de captura front doc
        if ("startFrontDocument".equals(action)) {
            //Argumento 0 es el ID de licencia de Codigo de barras
            //Argumento 1 es el Password para Ofuscar! Si va Blanco, no Ofusca
            Intent intent = new Intent(this.cordova.getActivity().getApplicationContext(), DetectorActivity.class);
            intent.putExtra(BaseScanActivity.EXTRAS_LICENSE_KEY, args.getString(0));
            intent.putExtra("EXTRAS_LICENSEE", args.getString(1).equals("") ? null : args.getString(1));
            intent.putExtra("EXTRAS_TIMEOUT", args.getString(2));
            this.cordova.startActivityForResult((CordovaPlugin) this, intent, MY_REQUEST_CODE_FRONT);
            bStatus = true;
        }
        //Si llega el mensaje de Inicio de la Captura de la Huella
        if ("startFingerprint".equals(action)) {
            //Argumento 0 es el Numero de dedos del 1 al 7
            //Argumento 1 es el Numero de Capturas (1 o 2)
            //Argumento 2 es el Password para Ofuscar! Si va Blanco, no Ofusca
            //startFingerprint(args.getString(0),args.getString(1));
            //se hace la llamada del inten con retorno del resultado para la muestra del mismo en json sobre la webview
            Intent intent = new Intent(this.cordova.getActivity().getApplicationContext(), FbCaptureActivity.class);
            intent.putExtra("TipoHuella", args.getString(0));
            intent.putExtra("EXTRAS_LICENSEE", args.getString(2).equals("") ? null : args.getString(2));
            intent.putExtra("EXTRAS_TIMEOUT", args.getString(3));
            this.cordova.startActivityForResult((CordovaPlugin) this, intent, MY_REQUEST_CODE_BIOMETRIC);
            bStatus = true;
        }
        //Si llega el mensaje de Inicio del  licenciamiento de morpho
        if ("startMorphoLicense".equals(action)) {
            Intent intent = new Intent(this.cordova.getActivity().getApplicationContext(), LicenseActivity.class);
            this.cordova.startActivityForResult((CordovaPlugin) this, intent, MY_REQUEST_CODE_LICENSE);
            bStatus = true;
        }
        // Si llega el mensaje de Inicio de  selphi
        if ("startFaceCapture".equals(action)) {
            //Argumento 1 es el Password para Ofuscar! Si va Blanco, no Ofusca
            Intent intent = new Intent(this.cordova.getActivity().getApplicationContext(), FmCaptureActivity.class);
            intent.putExtra("EXTRAS_LICENSEE", args.getString(1).equals("") ? null : args.getString(1));
            intent.putExtra("EXTRAS_TIMEOUT", args.getString(2));
            intent.putExtra("EXTRAS_FACECAPTURE",args.getString(3).equals("") ? null : args.getString(3));
            this.cordova.startActivityForResult((CordovaPlugin) this, intent, MY_REQUEST_CODE_BIOMETRIC_FACE);
            bStatus = true;
        }
        //Si debo enviar la informacion al Servidor
        if ("sendFingerprintProcess".equals(action)) {
            proceso = 1;
            consumo = new mp_huellas("2", args.getString(7), "Pruebas1234", args.getString(1), args.getString(3), args.getString(4), args.getString(5), args.getString(6));
            a var2 = new a();
            var2.execute();
            bStatus = true;
        }
        //Si debo enviar la informacion al Servidor
        if ("sendFaceProcess".equals(action)) {
            proceso = 2;
            a var2 = new a();
            var2.execute();
            bStatus = true;
        }
        //Si debo enviar la informacion al Servidor
        if ("sendAutenticacionDocumentoProcess".equals(action)) {
            proceso = 3;
            consumo = new mp_huellas("2", args.getString(1), args.getString(9), args.getString(3), args.getString(5), args.getString(6), args.getString(7), args.getString(8), args.getString(2));
            a var2 = new a();
            var2.execute();
            bStatus = true;
        }
        //Si debo enviar la informacion al Servidor
        if ("sendOCRProcess".equals(action)) {
            //proceso = 3;
            a var2 = new a();
            var2.execute();
            bStatus = true;
        }//envio info para match selfi y doc con numero de doc
        if ("sendMatchDocumentoFaceProcess".equals(action)) {
            consumoface= new mp_face(args.getString(1),args.getString(4));
            proceso = 4;
            a var2 = new a();
            var2.execute();
            bStatus = true;


        }//envio info para autenticidad del documento con rostro
        if ("sendAutenticacionDocumentoFaceProcess".equals(action)) {

            /*try {
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inPreferredConfig = Bitmap.Config.ARGB_8888;
                Bitmap a = BitmapFactory.decodeStream(new FileInputStream(args.getString(2)), null, options);
                a=BitmapFactory.decodeStream(new FileInputStream(args.getString(3)), null, options);
                a=BitmapFactory.decodeStream(new FileInputStream(args.getString(5)), null, options);
                //a=BitmapFactory.decodeStream(new FileInputStream(args.getString(6)), null, options);
            } catch (IOException e) {
                e.printStackTrace();
            }*/
            consumoface= new mp_face(args.getString(1),args.getString(6),args.getString(3),args.getString(2),args.getString(5));
            proceso = 5;
            a var2 = new a();
            var2.execute();
            bStatus = true;

        }

        return bStatus;
    }

    /*
         CallBack de captura de Huella
     */
    private void callBackFingerprint(String sMensaje) {
        //Envio CallBack de retorno
        //JSON ya armado por el SDK
        PluginResult result = new PluginResult(PluginResult.Status.OK, sMensaje);
        result.setKeepCallback(true);
        localCallbackContext.sendPluginResult(result);
    }

    /*
       CallBack de la lectura del documento
   */
    private void callBackBarCode(String sMensaje) {
        //Envio CallBack de retorno
        //JSON ya armado por el codigo de barras desde el SDK
        PluginResult result = new PluginResult(PluginResult.Status.OK, sMensaje);
        result.setKeepCallback(true);
        localCallbackContext.sendPluginResult(result);
    }

    //Funcion ECHO Test Inicial
    //String de Entrada
    //CallBack de retorno!
    private void echo(String msg) {
        //Si no viene nada!
        if (msg == null || msg.length() == 0) {
            localCallbackContext.error("Sin Mensaje!");
        } else {
            //Genero el Toast!
            Toast.makeText(webView.getContext(), (msg + " Desde Plugin"), Toast.LENGTH_LONG).show();
            localCallbackContext.success(msg + " Desde Plugin");
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            String results;
            if (requestCode == MY_REQUEST_CODE && resultCode == BaseScanActivity.RESULT_OK) {
                results = data.getExtras().getString("InfoDocumento");
                callBackBarCode(results);
                android.util.Log.e("obDocumento ", results);
            } else if (requestCode == MY_REQUEST_CODE_BIOMETRIC && resultCode == BaseScanActivity.RESULT_OK) {
                results = data.getExtras().getString("InfoBiometric");
                callBackFingerprint(results);
                android.util.Log.e("obDocumento ", results);
            } else if (requestCode == MY_REQUEST_CODE_FRONT && resultCode == BaseScanActivity.RESULT_OK) {
                results = String.valueOf((data != null) ? data.getExtras().getString("pathimagefrontdoc") : "{}");
                Log.e(this, "Failed" + results);
                callBackBarCode(results);
            } else if (requestCode == MY_REQUEST_CODE_LICENSE && resultCode == BaseScanActivity.RESULT_OK) {
                results = data.getExtras().getString("License");
                Log.e(this, "Failed" + results);
                callBackBarCode(results);
            } else if (requestCode == MY_REQUEST_CODE_BIOMETRIC_FACE && resultCode == BaseScanActivity.RESULT_OK) {
                results = data.getExtras().getString("InfoimgRostro");
                Log.e(this, "Failed" + results);
                callBackBarCode(results);
            } else if (requestCode == MY_REQUEST_CODE_FRONT && resultCode == BaseScanActivity.RESULT_CANCELED) {
                results = String.valueOf((data != null) ? data.getExtras().getString("pathimagefrontdoc") : "{}");
                Log.e(this, "Failed" + results);
                callBackBarCode(results);
            } else if (requestCode == MY_REQUEST_CODE_BIOMETRIC_FACE && resultCode == BaseScanActivity.RESULT_CANCELED) {
                results = String.valueOf((data != null) ? data.getExtras().getString("InfoimgRostro") : "{}");
                Log.e(this, "Failed" + results);
                callBackBarCode(results);
            } else if (requestCode == MY_REQUEST_CODE_LICENSE && resultCode == BaseScanActivity.RESULT_CANCELED) {
                results = String.valueOf((data != null) ? data.getExtras().getString("License") : "{}");
                Log.e(this, "Failed" + results);
                callBackBarCode(results);
            } else if (requestCode == MY_REQUEST_CODE && resultCode == BaseScanActivity.RESULT_CANCELED) {
                results = String.valueOf((data != null) ? data.getExtras().getString("InfoDocumento") : "{}");
                Log.e(this, "Failed" + results);
                callBackBarCode(results);
            } else if (requestCode == MY_REQUEST_CODE_BIOMETRIC && resultCode == BaseScanActivity.RESULT_CANCELED) {
                results = data.getExtras().getString("InfoBiometric");
                callBackFingerprint(results);
            }
        } catch (NullPointerException e) {
            Log.e(this, e, "Failed to set preview display!");
        } catch (Exception e) {
            Log.e(this, e, "Failed to set preview display!");
        }
    }

    private class a extends AsyncTask<Object, Object, String> {
        private boolean b;

        private a() {
            this.b = false;
        }

        @Override
        protected String doInBackground(Object... params) {

            String URL="https://portal.bytte.com.co/casb/ProcesoAutenticacion/V3.5/CASB.ProcesoAutenticacionRest/api/";

            f var3 = null;
            if (proceso == 1) {
                var3 = bytte.biometric.com.byttelibrary.b.a.a(URL+"/MPFingerprint", consumo);

            } else if (proceso == 4) {
                var3 = bytte.biometric.com.byttelibrary.b.a.b(URL+"MPMatchDocumentoFace",consumoface);

            } else if (proceso == 3) {
                var3 = bytte.biometric.com.byttelibrary.b.a.a("https://portal.bytte.com.co/casb/ProcesoAutenticacion/CASB.ProcesoAutenticacionRest/api/MPValidacionDocumento", consumo);

            } else if (proceso == 5) {
                var3 = bytte.biometric.com.byttelibrary.b.a.a(URL+"MPValidacionDocumentoFace", consumoface);

            }
            callBackBarCode(String.valueOf(var3.b()));
            return var3.b();

        }

        protected void onPreExecute() {
            super.onPreExecute();
        }

        protected f<b> a(Void... var1) {
            String var2 = "https://portal.bytte.com.co/casb/CuentaDigital/CuentaDigitalRest/api/MPData/";
            f var3 = bytte.biometric.com.byttelibrary.b.a.a(var2);
            return var3;
        }

        protected void a(f<b> var1) {
            try {
                b var2 = (b) var1.c();
            } catch (Exception var3) {
                var3.printStackTrace();
            }

        }

    }


}
