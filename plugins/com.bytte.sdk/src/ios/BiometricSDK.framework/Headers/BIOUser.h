//
//  BIOUser.h
//  BiometricSDK
//
//  Created by WASAG Kamil - EXT-REPERIO (MORPHO) on 06/05/2016.
//  Copyright © 2016 WASAG Kamil - EXT-REPERIO (MORPHO). All rights reserved.
//

#import <Foundation/Foundation.h>
#import <BiometricSDK/BIOBiometrics.h>

/**
 *  @author Morpho
 *
 *  @brief Object hold inforation about of owner of templates
 */
@interface BIOUser: NSObject


/**
 *  @author Morpho
 *
 *  @brief Name of user.
 */
@property (nullable, nonatomic, retain) NSString *name;

/**
 *  @author Morpho
 *
 *  @brief UUID of user - this property is used to pair user with template
 */
@property (nullable, nonatomic, retain) NSString *uuid;

@end
