//
//  LKMSLicense.h
//  LKMS
//
//  Created by WASAG Kamil - EXT-REPERIO (MORPHO) on 23/06/2016.
//  Copyright © 2016 MORPHO. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LKMSLicense : NSObject<NSCoding>

+ (instancetype)getLicenseFromEncryptedData:(NSData*)data withLicenseID:(NSString*)licenseID withError:(NSError **)error;
- (instancetype)initLicenseID:(NSString*)licenseID withData:(NSData *)data;
- (bool)saveAndEncryptWithKey:(NSString*)key toFile:(NSURL*)filePath;
+ (instancetype)readAndDecryptWithKey:(NSString*)key toFile:(NSURL*)filePath;
@property (nonatomic, strong, readonly) NSString* licenseID;
@property (nonatomic, strong, readonly) NSData* licenseData;
@property (nonatomic, strong, readonly) NSArray<NSString*>* features;

@end
