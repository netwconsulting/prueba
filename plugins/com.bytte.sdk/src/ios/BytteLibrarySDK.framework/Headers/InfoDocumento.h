//
//  InfoDocumento.h
//  BytteLibrarySDK
//
//  Created by Allan Diaz on 12/19/16.
//  Copyright © 2016 Allan Diaz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import "BaseResponse.h"

@interface InfoDocumento : BaseResponse

@property (nonatomic) NSString * VersionCedula;
@property (nonatomic) NSString * NumeroTarjeta;
@property (nonatomic) NSString * NumeroCedula;
@property (nonatomic) NSString * PrimerApellido;
@property (nonatomic) NSString * SegundoApellido;
@property (nonatomic) NSString * PrimerNombre;
@property (nonatomic) NSString * SegundoNombre;
@property (nonatomic) NSString * NombresCompletos;
@property (nonatomic) NSString * Sexo;
@property (nonatomic) NSString * FechaNacimiento;
@property (nonatomic) NSString * RH;
@property (nonatomic) NSString * TipoDedo1;
@property (nonatomic) NSString * TipoDedo2;
@property             NSData   * barcodeRaw;
@property             NSString * barcodeBase64;
@property             NSString * pathImagen;
@property             UIImage  * ImageSample;
@property             NSData   * ImageData;

//Convierte el Objeto a JSON
-(NSString *) Convert2JSONValue:(NSString *) password;

@end



