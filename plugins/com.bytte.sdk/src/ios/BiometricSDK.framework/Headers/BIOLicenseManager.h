//
//  BIOLicenseManager.h
//  BiometricSDK
//
//  Created by WASAG Kamil - EXT-REPERIO (MORPHO) on 11/05/2016.
//  Copyright © 2016 WASAG Kamil - EXT-REPERIO (MORPHO). All rights reserved.
//

#import <Foundation/Foundation.h>
#import <BiometricSDK/BIOLicense.h>
#import <BiometricSDK/BIOLicenseOptions.h>


/**
 *  @author Morpho
 *
 *  @brief Object that handle operations related to license handling.
 */
@interface BIOLicenseManager : NSObject

/**
 *  @author Morpho
 *
 *  @brief Property that gets the information about current license, nil in case there is no license
 */
@property (nonatomic, strong, readonly) BIOLicense* license;


/**
 *  @author Morpho
 *
 *  @brief  Property that enables/disables logging from license updates
 */
@property (nonatomic, getter=isLogEnabled) BOOL logEnabled;

/**
 *  @author Morpho
 *
 *  @brief Method remove current license
 *
 *  @return nil if license is completly remove oherwise NSError
 */
- (NSError *)removeLicense;

/**
 *  @author Morpho
 *
 *  @brief Method that activate BiometricSDK and retrive license
 *
 *  @param options           Object contains information about comunication with server and server
 *  @param completionHandler Block of code that will be run when comunication with license server ends. Return license in case of success, otherwise error.
 */
- (void)activateWithOptions:(BIOLicenseOptions*)options
      withCompletionHandler:(void(^)(BIOLicense* License, NSError* error))completionHandler;


@end
