//
//  MBSSProcessResponse.h
//  BytteLibrarySDK
//
//  Created by Allan Diaz on 12/19/17.
//  Copyright © 2017 Allan Diaz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import "BaseResponse.h"

@interface MBSSProcessResponse : BaseResponse

@property (nonatomic) NSNumber * ScoreObtenido;
@property (nonatomic) bool Autenticado;

-(NSString *) Convert2JSONValue;

@end
