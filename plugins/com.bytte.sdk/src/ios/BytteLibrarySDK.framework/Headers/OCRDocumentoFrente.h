//
//  OCRDocumentoFrente.h
//  BytteLibrarySDK
//
//  Created by Allan Diaz on 8/29/17.
//  Copyright © 2017 Allan Diaz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>

@interface OCRDocumentoFrente : NSObject

@property (nonatomic) NSString * TituloRepublicaColombia;
@property (nonatomic) NSString * TituloIdentificacionPersonal;
@property (nonatomic) NSString * TituloCedulaCiudadania;
@property (nonatomic) NSString * TituloApellidos;
@property (nonatomic) NSString * TituloNombres;
@property (nonatomic) NSString * TituloFirma;
@property (nonatomic) NSString * NumeroDocumento;
@property (nonatomic) NSString * Nombres;
@property (nonatomic) NSString * Apellidos;

@end
