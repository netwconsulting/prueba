//
//  BIOLicenseOptions.h
//  BiometricSDK
//
//  Created by WASAG Kamil - EXT-REPERIO (MORPHO) on 28/04/2016.
//  Copyright © 2016 WASAG Kamil - EXT-REPERIO (MORPHO). All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 *  Object used activation license proces, during comunication with server
 */
@interface BIOLicenseOptions : NSObject


/**
 *  Data used by server to activate BiometricSDK, NOTE: this block of data is provided by client.
 */
@property (nonatomic, strong, nonnull) NSData *activationData;

/**
 *  String with address to license server
 */
@property (nonatomic, strong, nonnull) NSString *serverURL;

@end
