//
//  FingerprintResult.h
//  BytteLibrarySDK
//
//  Created by Allan Diaz on 12/20/16.
//  Copyright © 2016 Allan Diaz. All rights reserved.
//
#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

/*!
 Resultado de la toma de las huellas por el modulo Fingerprint!
 */
@interface FingerprintResult : NSObject
/*!
 * @brief The source image.
 */
@property UIImage *fSourceImage;
/*!
 * @brief The processed image.
 */
@property UIImage *fProcessedImage;
/*!
 * @brief The enhanced image.
 */
@property UIImage *fEnhancedImage;
/*!
 * @brief The inverted and y-axis flipped processed image.
 */
@property UIImage *fInvertedMirroredProcessedImage;
/*!
 * @brief The fingerprint template, to be used with Onyx matchers.
 */
@property NSData *fFingerprintTemplate;
/*!
 * @brief The WSQ data.
 */
@property NSData *fWSQ;
/*!
 * @brief The inverted and mirrored WSQ data.
 */
@property NSData *fInvertedMirroredWSQ;
/*!
 * @brief Black and white image of the processed image.
 */
@property UIImage *fBlackWhiteProcessed;
/*!
 * @brief The quality score. Any score over 15 is acceptable.
 */
@property float fQuality;
/*!
 * @brief The focus measure score. [0, 1] 0.1 is acceptable.
 */
@property float fFocusMeasure;
/*!
 * @brief The nfiqscore of the WSQ file.
 */
@property int fNfiqscore;
/*!
 * @brief The mlscore of the WSQ file.
 */
@property float fMlpscore;
/*!
 * @brief The direction of the finger.
 * Images will be flipped upright beforehand.
 */
@property NSInteger fFingerDirection;
/*!
 * @brief The finger number.
 */
@property NSInteger fFinger;
/*!
 * @brief image size for all returned images.
 */
@property CGSize fSize;
    

@property NSString * PathWSQ;
@property NSString * PathBitmap;
@property NSString * ImageTemplate;

@property (nonatomic) int      iStride;
@property (nonatomic) uint32_t uiWidth;
@property (nonatomic) uint32_t uiHeight;
@property (nonatomic) float    fResolution;
@property (nonatomic) BOOL     bAlive;
@property (nonatomic) int      imageQuality;
@property (nonatomic) int      iCaptura;
@property (nonatomic) NSData  *bBuffer;
@property (nonatomic) BOOL     bStatus;


//Convierte a JSON
+(NSString *) Convert2JSONValue:(NSArray *)fingerprintsObjects;

@end

