//
//  BioSDKLicence.h
//  BytteLibrarySDK
//
//  Created by Allan Diaz on 7/27/17.
//  Copyright © 2017 Allan Diaz. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LicenseMorphoResponse.h"
#import "BaseResponse.h"


@protocol BioSDKLicenceDelegate <NSObject>
@optional
- (void)doBioSDKLicenceDelegate:(LicenseMorphoResponse *)statusLicence;
- (void)doCammeraPermissionsDelegate:(BaseResponse *)statusCamera;
@end

@interface BioSDKLicence : NSObject
-(bool)InitLicenceManager;
-(void)GetCammeraPermissions;

//Referencia al Delegado
@property (nonatomic, weak) id <BioSDKLicenceDelegate> bioSDKLicenceDelegate;

@end
