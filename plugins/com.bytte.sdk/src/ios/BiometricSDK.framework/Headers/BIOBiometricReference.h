//
//  BIOBiometricReference.h
//  BiometricSDK
//
//  Created by RIBEIRO Tiago on 10/04/17.
//  Copyright © 2017 MORPHO. All rights reserved.
//

#import <BiometricSDK/BIOBiometricSet.h>

@interface BIOBiometricReference : BIOBiometricSet

@property (nonatomic, strong, readonly, nonnull) NSString *userUUID;

- (instancetype _Nonnull)initWithUserUUID:(NSString * _Nonnull)userUUID;

@end
