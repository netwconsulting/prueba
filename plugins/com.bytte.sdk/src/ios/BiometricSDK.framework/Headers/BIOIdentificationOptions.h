//
//  BIOIdentificationOptions.h
//  BiometricSDK
//
//  Created by WASAG Kamil - EXT-REPERIO (MORPHO) on 04/05/2016.
//  Copyright © 2016 WASAG Kamil - EXT-REPERIO (MORPHO). All rights reserved.
//

#import <Foundation/Foundation.h>
#import <BiometricSDK/BIOMatchingOptions.h>

/**
 *  Object use to configure BIOMatcherHandler during identification process
 */

@interface BIOIdentificationOptions : BIOMatchingOptions

@end
