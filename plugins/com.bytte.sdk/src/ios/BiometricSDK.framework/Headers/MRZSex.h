//
//  MRZSex.h
//  BiometricSDK
//
//  Created by RIBEIRO Tiago on 28/04/17.
//  Copyright © 2017 MORPHO. All rights reserved.
//

typedef NS_ENUM(NSUInteger, MRZSex) {
    MRZSexMale = 0,
    MRZSexFemale = 1,
    MRZSexUnspecified = 2,
    
    MRZSexUnknown = -1
};

MRZSex mrzSexFromCharacter(char character, NSError * __nullable * __nullable error);
NSString * __nonnull NSStringFromMRZSex(MRZSex mrzSex);
