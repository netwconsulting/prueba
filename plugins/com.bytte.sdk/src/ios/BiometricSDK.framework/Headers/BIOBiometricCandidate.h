//
//  BIOBiometricCandidate.h
//  BiometricSDK
//
//  Created by RIBEIRO Tiago on 10/04/17.
//  Copyright © 2017 MORPHO. All rights reserved.
//

#import <BiometricSDK/BIOBiometricSet.h>

@interface BIOBiometricCandidate : BIOBiometricSet

@end
