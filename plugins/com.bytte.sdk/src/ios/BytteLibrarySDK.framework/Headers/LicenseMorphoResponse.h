//
//  LicenseMorphoResponse.h
//  BytteLibrarySDK
//
//  Created by Allan Diaz on 9/4/17.
//  Copyright © 2017 Allan Diaz. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LicenseMorphoResponse : NSObject

@property (weak, nonatomic) NSString * licenseId;
@property (weak, nonatomic) NSString * licenseFeatures;
@property (weak, nonatomic) NSString * licenceErrorMessage;
@property bool licenceStatusOK;

//Convierte a JSON
-(NSString *) Convert2JSONValue;
@end
