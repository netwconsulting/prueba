//
//  BIOImage+RegionCropping.h
//  BiometricSDK
//
//  Created by RIBEIRO Tiago on 07/03/17.
//  Copyright © 2017 MORPHO. All rights reserved.
//

#import <BiometricSDK/BIOImage.h>
#import <BiometricSDK/BIODocumentTrackingInformation.h>

@interface BIOImage (RegionCropping)

+ (instancetype _Nullable)cropImage:(BIOImage * _Nonnull)bioImage toRegion:(BIODocumentTrackingInformation * _Nonnull) documentTrackingInfo;

@end
