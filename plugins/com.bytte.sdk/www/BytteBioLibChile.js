/*
 ||---------------------------------------------------------------------
 //Libreria dedicada al documento de identidad Chileno Version MRZ

 BytteBioLib - Objective C         - Xcode              Allan Diaz :)
 BytteBioLib - Android Java        - Android Studio     Venancio Prada
 BytteBioLib - JavaScript Cordova  - Visual Studio Code Allan Diaz
 */

var exec = require('cordova/exec');

//Funcion de Test
exports.echo = function(arg0, success, error) {
    exec(success, error, "BytteBioLib", "echo", [arg0]);
};

//Inicia la Licencia del componente Morpho
exports.startMorphoLicense = function(success, error) {
    exec(success, error, "BytteBioLib", "startMorphoLicense", ['']);
};

//Inicia la Captura de la huella
//arg0 Numero de Dedo (1 al 10)
//arg1 Numero de Capturas
//arg2 KeyPass
//arg3 TimeOut 
exports.startFingerprint = function(arg0, arg1, arg2, arg3, success, error) {
    exec(success, error, "BytteBioLib", "startFingerprint", [arg0, arg1, arg2, arg3]);
};

//Inicia la Captura del Frente del Documento!
//arg0 Codigo Activacion Licencia
exports.startChileFrontDocument = function(arg0, success, error) {
    exec(success, error, "BytteBioLib", "startChileFrontDocument", [arg0]);
};

//Inicia la Captura del Reverso del Documento!
//arg0 Codigo Activacion Licencia
exports.startChileReversoMRZDocument = function(arg0, success, error) {
    exec(success, error, "BytteBioLib", "startChileReversoMRZDocument", [arg0]);
};

//Inicia la Captura del Selfie
//arg0 Minucia para Autenticacion
//arg1 KeyPass
//arg2 TimeOut
//arg3 FaceCaptureMode
exports.startFaceCapture = function(arg0, arg1, arg2, arg3, success, error) {
    exec(success, error, "BytteBioLib", "startFaceCapture", [arg0, arg1, arg2, arg3]);
};

//Proceso de Envio de Informacion al Servidor
//Para Match facial del Documento Vs Rostro del usuario
//arg0  :  Id del Proceso
//arg1  :  NumeroDocumento
//arg2  :  Imagen del Frente del Documento
//arg3  :  Path Imagen Selfie Usuario
//arg4  :  FilePwd -> Password con que se ofuscaron las imagenes -> ''
exports.sendMatchDocumentoFaceProcessBase = function(arg0, arg1, arg2, arg3, arg4, success, error) {
    exec(success, error, 
                  "BytteBioLib", 
                  "sendMatchDocumentoFaceProcessBase", 
                  [arg0, arg1, arg2, arg3, arg4]);
};

//Proceso de Envio de Informacion al Servidor
//Para Match facial del Documento Vs Rostro del usuario
//arg0  :  NumeroDocumento
exports.sendConsultaUsuarioEnrolado = function(arg0, success, error) {
    exec(success, error, 
                  "BytteBioLib", 
                  "sendConsultaUsuarioEnrolado", 
                  [arg0]);
};

//Enrolamiento Facial unicamente MBSS
//Proceso de Envio de Informacion al Servidor para enrolamiento
//Para Match facial del Documento Vs Rostro del usuario
//arg0  :  Id del Proceso
//arg1  :  NumeroDocumento
//arg2  :  Path Imagen Selfie Usuario
//arg3  :  FilePwd -> Password con que se ofuscaron las imagenes -> ''
exports.sendEnrolamientoFaceMBSS = function(arg0, arg1, arg2, arg3, success, error) {
    exec(success, error, 
                  "BytteBioLib", 
                  "sendEnrolamientoFaceMBSS", 
                  [arg0, arg1, arg2, arg3]);
};

//Autenticacion Facial unicamente MBSS
//Proceso de Envio de Informacion al Servidor para enrolamiento
//Para Match facial del Documento Vs Rostro del usuario
//arg0  :  Id del Proceso
//arg1  :  NumeroDocumento
//arg2  :  Path Imagen Selfie Usuario
//arg3  :  FilePwd -> Password con que se ofuscaron las imagenes -> ''
exports.sendAutenticacionFaceMBSS = function(arg0, arg1, arg2, arg3, success, error) {
    exec(success, error, 
                  "BytteBioLib", 
                  "sendAutenticacionFaceMBSS", 
                  [arg0, arg1, arg2, arg3]);
};