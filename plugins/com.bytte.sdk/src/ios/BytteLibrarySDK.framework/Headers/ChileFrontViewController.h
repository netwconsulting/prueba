//
//  ChileFrontViewController.h
//  BytteLibrarySDK
//
//  Created by Allan Diaz on 11/28/17.
//  Copyright © 2017 Allan Diaz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FrontDocumentResponse.h"

@protocol CapturaChileFrontDocumentDelegate <NSObject>
@optional
- (void)doCapturaChileFrontDocumentDelegate:(FrontDocumentResponse *)frontResponse;
@end

@interface ChileFrontViewController : UIViewController

- (id)initWithKey:(NSString *)Key AndUIImageGuide:(UIImage *) imGuiaBase;
- (void)setSerialKey:(NSString *)serialKey WithImGuia:(UIImage *) imGuiaBase;

@property (strong, nonatomic) id <CapturaChileFrontDocumentDelegate> frontChileDocumentProcessDelegate;

@end
