//
//  BaseResponse.h
//  BytteLibrarySDK
//
//  Created by Allan Diaz on 12/19/16.
//  Copyright © 2016 Allan Diaz. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BaseResponse : NSObject
@property (nonatomic) NSString * MensajeOriginal;
@property (nonatomic) NSString * MensajeRetorno;
@property (nonatomic) NSString * CodigoOperacion;
@property (nonatomic) BOOL       StatusOperacion;
@property (nonatomic) NSString * IdProceso;
@end
