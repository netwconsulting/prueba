/*
 BytteBioLib - Objective C - Xcode
 Allan Diaz :)
 Julio de 2016
 
 Script con las funciones requeridas para implementar el
 SDK de Bytte Biometrico y Captura codigo de barras desde
 dispositivos IOS Iphone
 Bytte 2017 
 */

#import <Cordova/CDV.h>
#import <BytteLibrarySDK/BioSDKLicence.h>
#import <BytteLibrarySDK/LicenseMorphoResponse.h>
#import <BytteLibrarySDK/CameraViewController.h>
#import <BytteLibrarySDK/CameraFrontViewController.h>
#import <BytteLibrarySDK/FingerprintViewController.h>
#import <BytteLibrarySDK/FingerprintBaseViewController.h>
#import <BytteLibrarySDK/FingerprintResult.h>
#import <BytteLibrarySDK/FingerprintCaptureResult.h>
#import <BytteLibrarySDK/BioSDKFingerprintViewController.h>
#import <BytteLibrarySDK/SendDataProcess.h>
#import <BytteLibrarySDK/BioSDKFaceViewController.h>
#import <BytteLibrarySDK/Util.h>
#import <BytteLibrarySDK/FrontDocumentResponse.h>
#import <BytteLibrarySDK/ChileFrontViewController.h>
#import <BytteLibrarySDK/ChileBackViewController.h>

@interface BytteBioLib : CDVPlugin<CapturaBarCodeDelegate,
CapturaFingerprintDelegate,
SendDataProcessDelegate,
BioSDKLicenceDelegate,
CapturaFrontDocumentDelegate,
BioSDKFaceDelegate,
FingerprintProcessDelegate,
CapturaChileFrontDocumentDelegate,
CapturaMRZChileCodeDelegate>

- (void)echo:(CDVInvokedUrlCommand*)command;
- (void)startBarCode:(CDVInvokedUrlCommand*)command;
- (void)startFingerprint:(CDVInvokedUrlCommand*)command;
- (void)startMorphoLicense:(CDVInvokedUrlCommand*)command;
- (void)startFrontDocument:(CDVInvokedUrlCommand*)command;
- (void)startFaceCapture:(CDVInvokedUrlCommand*)command;
@property (strong, nonatomic) CDVInvokedUrlCommand* latestCommand;
@property BOOL hasPendingOperation;


@end
