//
//  BIOCapturingInfo.h
//  BiometricSDK
//
//  Created by WASAG Kamil - EXT-REPERIO (MORPHO) on 06/04/2016.
//  Copyright © 2016 WASAG Kamil - EXT-REPERIO (MORPHO). All rights reserved.
//

/**
 *   Type of flags returns as information user what action should he do to capture image.
 */
typedef int BIOCapturingInfo;

extern const BIOCapturingInfo BIOCapturingInfoUndefined;

/**
 *  Flag is send whenewer user must get out of the camera field
 */
extern const BIOCapturingInfo BIOCapturingInfoFaceInfoGetOutField;

/**
 *  Flag is send whenewer user must come back in the camera field
 */
extern const BIOCapturingInfo BIOCapturingInfoFaceInfoComeBackField;

/**
 *  Flag is send whenewer user must come back in the camera field
 */
extern const BIOCapturingInfo BIOCapturingInfoFaceInfoTurnLeft;

/**
 * Flag is send whenewer user must turn head right
 */
extern const BIOCapturingInfo BIOCapturingInfoFaceInfoTurnRight;

/**
 * Flag is send whenewer user must face center but turn head left
 */
extern const BIOCapturingInfo BIOCapturingInfoFaceInfoCenterTurnLeft;

/**
 * Flag is send whenewer user must face center but turn head right
 */
extern const BIOCapturingInfo BIOCapturingInfoFaceInfoCenterTurnRight;

/**
 * Flag is send whenewer user must face center but rotate head down
 */
extern const BIOCapturingInfo BIOCapturingInfoFaceInfoCenterRotateDown;

/**
 * Flag is send whenewer user must face center but rotate head up
 */
extern const BIOCapturingInfo BIOCapturingInfoFaceInfoCenterRotateUp;

/**
 * Flag is send whenewer user must face center but tilt head left
 */
extern const BIOCapturingInfo BIOCapturingInfoFaceInfoCenterTiltLeft;

/**
 * Flag is send whenewer user must face center but tilt head right
 */
extern const BIOCapturingInfo BIOCapturingInfoFaceInfoCenterTiltRight;

/**
 * Flag is send whenewer user blocked with unsuccesful attemts
 */
extern const BIOCapturingInfo BIOCapturingInfoFaceInfoTimeOut;

/**
 * Flag is send whenewer user must move foreward to camera
 */
extern const BIOCapturingInfo BIOCapturingInfoFaceInfoCenterMoveForwards;

/**
 * Flag is send whenever user unsuccesful attemts
 */
extern const BIOCapturingInfo BIOCapturingInfoFaceInfoUnsuccessfulAttempt;

/**
 * Flag is send whenewer user must move backward from camera;
 */
extern const BIOCapturingInfo BIOCapturingInfoFaceInfoCenterMoveBackwards;

/**
 * Flag is send whenewer user must look in front of camera;
 */
extern const BIOCapturingInfo BIOCapturingInfoFaceInfoCenterLookFrontOfCamera;

/**
 *Flag is send whenewer user must look camera with less movement
 */
extern const BIOCapturingInfo BIOCapturingInfoFaceInfoCenterLookCameraWithLessMovement;

/**
 * Flag is send whenewer user must turn head left and then right
 */
extern const BIOCapturingInfo BIOCapturingInfoFaceInfoTurnLeftRight;

/**
 * Flag is send whenewer user must turn head down.
 */
extern const BIOCapturingInfo BIOCapturingInfoFaceInfoTurnDown;

/**
 * Flag is send whenewer user turns the head too fast
 */
extern const BIOCapturingInfo BIOCapturingInfoFaceInfoTooFast;


/**
 * Function translate BIOCapturingInfo to english strings
 */
NSString* NSStringFromBIOCapturingInfo(BIOCapturingInfo faceRequest);
