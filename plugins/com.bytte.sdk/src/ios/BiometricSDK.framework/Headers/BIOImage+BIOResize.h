//
//  BIOImage+BIOResize.h
//  BiometricSDK
//
//  Created by WASAG Kamil - EXT-REPERIO (MORPHO) on 23/12/2016.
//  Copyright © 2016 MORPHO. All rights reserved.
//

#import <BiometricSDK/BiometricSDK.h>

@interface BIOImage (BIOResize)
- (NSData*)compressImageWithQuality:(uint32_t)quality;
- (NSData*)compressImageToSizeInKilobytes:(CGFloat)sizeInKilobytes;
- (BIOImage *)compressImageToWSQRatio:(CGFloat)ratio withScannerBlack:(Byte)scannerBlack andScannerWhite:(Byte)scannerWhite;
- (BIOImage *)compressImageToJPEG2000InKilobytes:(CGFloat)maximumSizeInKilobytes;

@end
