//
//  BIOLicenseStatus.h
//  BiometricSDK
//
//  Created by WASAG Kamil - EXT-REPERIO (MORPHO) on 04/05/2016.
//  Copyright © 2016 WASAG Kamil - EXT-REPERIO (MORPHO). All rights reserved.
//


/**
 *  Enum type that give status of curent license
 */
typedef NS_ENUM(NSUInteger, BIOLicenseStatus) {
    /**
     *  License is not valid, renew license or contact with service provider
     */
    BIOLicenseStatusValid,
    /**
     *  There is unknow status, try again or contact with service provider
     */
    BIOLicenseStatusUnknown
};
