//
//  FingerprintBaseViewController.h
//  BytteLibrarySDK
//
//  Created by Allan Diaz on 7/10/17.
//  Copyright © 2017 Allan Diaz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import <MediaPlayer/MediaPlayer.h>
#import <UIKit/UIKit.h>
#import "InfoDocumento.h"

@protocol CapturaFingerprintDelegate <NSObject>
@optional
- (void)doCapturaFingerprintDelegate:(NSArray *)fingerprintsObjects;
@end

@interface FingerprintBaseViewController : UIViewController

- (id)initGuia:(UIImage *) imgGuia;
- (void)iniciaCapturaHuella;

@property int numeroHuellasCaptura;
@property int numeroDedo;
@property (strong, nonatomic)  UIViewController * parentViewController;
@property (strong, nonatomic)  id <CapturaFingerprintDelegate> capturaFingerprintDelegate;
@end
