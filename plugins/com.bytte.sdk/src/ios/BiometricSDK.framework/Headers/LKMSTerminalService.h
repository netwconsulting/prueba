//
//  LKMSClient.h
//  LKMS
//
//  Created by WASAG Kamil - EXT-REPERIO (MORPHO) on 24/06/2016.
//  Copyright © 2016 MORPHO. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LKMSLicense.h"

@interface LKMSTerminalService : NSObject

@property (nonatomic,readonly) LKMSLicense *license;
- (void)createLicenseServerURLstring:(NSString*)URLString
                  withActivationDate:(NSData*)data
               withCompletionHandler:(void(^)(NSError*error))completionHandler;
- (NSError*)removeLicense;
+ (LKMSTerminalService*)sharedInstance;
+ (void)setLKMSLogsEnabled:(bool)enabled;
+ (bool)LKMSLogsEnabled;
@end
