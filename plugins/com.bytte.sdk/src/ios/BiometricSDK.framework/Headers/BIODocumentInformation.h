//
//  BIODocumentInformation.h
//  BiometricSDK
//
//  Created by WASAG Kamil - EXT-REPERIO (MORPHO) on 25/11/2016.
//  Copyright © 2016 MORPHO. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <BiometricSDK/BIODocLocation.h>


/**
 *  @author Morpho
 *
 *  @brief Object provides basic information about taken infromation from document
 */
@interface BIODocumentInformation : NSObject

/**
 *  @author Morpho
 *
 *  @brief Initialize that feel location parameter in BIODocumentInformation object
 *
 *  @param location location of inforamtion in document
 *
 *  @return BIODocumentInformation object with given information prefilled
 */
- (instancetype _Nonnull)initWithLocation:(BIODocLocation)location;

/**
 *  @author Morpho
 *
 *  @brief Flag that tell where on document information is situated
 */
@property (nonatomic) BIODocLocation location;

@end
