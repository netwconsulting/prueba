//
//  FingerprintCaptureResult.h
//  BytteLibrarySDK
//
//  Created by Allan Diaz on 8/14/17.
//  Copyright © 2017 Allan Diaz. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseResponse.h"
#import "FingerprintResult.h"
#import "FingerprintCaptureResult.h"

@interface FingerprintCaptureResult : BaseResponse

@property (nonatomic) NSArray * fingerprintsCapture;
@property (nonatomic) UIImage  * ImagenFrenteDocumento;
@property (nonatomic) NSString * PathImagen;

//Convierte a JSON
-(NSString *) Convert2JSONValue:(NSString *) password;
@end
