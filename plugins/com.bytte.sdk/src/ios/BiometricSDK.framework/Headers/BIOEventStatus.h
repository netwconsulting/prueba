//
//  BIOEventStatus.h
//  BiometricSDK
//
//  Created by RIBEIRO Tiago on 01/08/2017.
//  Copyright © 2017 MORPHO. All rights reserved.
//

#ifndef BIOEventStatus_h
#define BIOEventStatus_h

typedef NS_ENUM(NSUInteger, BIOEventStatus) {
    BIOEventStatusSuccess,
    BIOEventStatusFailure
};

NSString * _Nonnull NSStringFromBIOEventStatus(BIOEventStatus status);
BIOEventStatus BIOEventStatusFromNSString(NSString * _Nonnull str, NSError * _Nullable * _Nullable error);


#endif /* BIOEventStatus_h */
