//
//  BIODiagnosticManager.h
//  BiometricSDK
//
//  Created by RIBEIRO Tiago on 19/07/2017.
//  Copyright © 2017 MORPHO. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <BiometricSDK/BIODiagnosticEvent.h>
#import <BiometricSDK/BIODiagnosticData.h>
#import <BiometricSDK/BIODiagnosticManagerDelegate.h>

@interface BIODiagnosticManager : NSObject

+ (int)version;
+ (instancetype _Nonnull)shared;

@property (nonatomic) id<BIODiagnosticManagerDelegate> _Nullable delegate;
@property (nonatomic, readonly) NSArray<id<BIODiagnosticEvent>> * _Nonnull events;

- (void)addEvent:(id<BIODiagnosticEvent> _Nonnull)event;
- (void)removeAllEvents;

- (BIODiagnosticData * _Nonnull)retrieveDiagnosticData;

@end
