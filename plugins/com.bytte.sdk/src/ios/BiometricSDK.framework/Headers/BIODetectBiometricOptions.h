//
//  BIODetectBiometricOptions.h
//  BiometricSDK
//
//  Created by WASAG Kamil - EXT-REPERIO (MORPHO) on 04/05/2016.
//  Copyright © 2016 WASAG Kamil - EXT-REPERIO (MORPHO). All rights reserved.
//

#import <BiometricSDK/BIOBiometrics.h>

/**
 *  Object use to configure BIOMatcherHandler during detection biometrics
 */

@interface BIODetectBiometricOptions : BIOBiometrics

@end
