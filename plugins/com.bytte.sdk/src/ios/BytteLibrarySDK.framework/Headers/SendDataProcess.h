//
//  SendDataProcess.h
//  BytteLibrarySDKTest
//
//  Created by Allan Diaz on 1/5/17.
//  Copyright © 2017 Allan Diaz. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ServerProcess.h"
#import "DocumentoInfoResponse.h"
#import "BaseResponse.h"
#import "OCRDocumentoFrente.h"
#import "OCRDocumentoReverso.h"
#import "OCRResponse.h"
#import "MBSSUserResponse.h"
#import "AutenticacionDocumentoResponse.h"
#import "MBSSProcessResponse.h"
#import "Util.h"

//Define el delegado para manejo de eventos
@protocol SendDataProcessDelegate <NSObject>
@optional
- (void)doProcesaFingerprint:(DocumentoInfoResponse *)baseResponse;
- (void)doProcesaOCRResponse:(OCRResponse *)OCRInfo;
- (void)doConsultaUsuario:(MBSSUserResponse *)baseResponse;
- (void)doMatchDocumento:(MBSSProcessResponse *)baseResponse;
- (void)doAutenticaFaceMBSS:(MBSSProcessResponse *)baseResponse;
- (void)doEnrolaFaceMBSS:(MBSSProcessResponse *)baseResponse;
- (void)doProcesaAutenticacionDocumento:(AutenticacionDocumentoResponse *)AutenticacionInfo;
@end


@interface SendDataProcess : NSObject


/* Procesa la Biometria de Huella Dactilar */
- (void)procesaBiometria:(NSString *)imgDocumento
             withHuella1:(NSString *)huella1
             withHuella2:(NSString *)huella2
             withBarCode:(NSString *)barCode
           withIdProcess:(NSString *)processID;


/* Procesa la Biometria de Huella Dactilar */
- (void)procesaBiometriaFingerprint:(NSData *)imgDocumento
                        withHuella1:(NSData *)huella1
                        withHuella2:(NSData *)Huella2
                        withBarCode:(NSData *)barCode
                      withIdProcess:(NSString *)processID;

/* Procesa la Biometria de Huella Dactilar */
- (void)procesaBiometriaFingerprintBitmap:(NSData *)imgDocumento
                              withHuella1:(NSData *)huella1
                              withHuella2:(NSData *)Huella2
                              withBarCode:(NSData *)barCode
                            withIdProcess:(NSString *)processID;

/* Procesa la Biometria de Huella Dactilar */
- (void)procesaBiometriaFingerprintBitmap:(NSData *)imgDocumento
                              withHuella1:(NSData *)huella1
                              withHuella2:(NSData *)huella2
                              withHuella3:(NSData *)huella3
                              withHuella4:(NSData *)huella4
                             withImageKey:(NSString *)imageKey
                              withBarCode:(NSData *)barCode
                      withNumeroDocumento:(NSString *)numeroDocumento
                            withIdProcess:(NSString *)processID;

/* Procesa la Biometria de Huella Dactilar */
- (void)procesaAutenticacionDocumento:(NSData *)imgDocumento
            withImagenDocumentoFrente:(NSData *)imgDocumentoFrente
                          withHuella1:(NSData *)huella1
                          withHuella2:(NSData *)huella2
                          withHuella3:(NSData *)huella3
                          withHuella4:(NSData *)huella4
                         withImageKey:(NSString *)imageKey
                          withBarCode:(NSData *)barCode
                  withNumeroDocumento:(NSString *)numeroDocumento
                        withIdProcess:(NSString *)processID;

/* Realiza el Match del Documento con el rostro */
- (void)procesaMatchDocumentoFace:(NSData *)imgSelfie
        withImagenDocumentoFrente:(NSData *)imgDocumentoFrente
                     withImageKey:(NSString *)imageKey
              withNumeroDocumento:(NSString *)numeroDocumento
                    withIdProcess:(NSString *)processID
                   withVersionAPI:(NSString *)versionAPI;

/* Realiza la Autenticacion del Documento con el rostro */
- (void)procesaAutenticacionDocumentoFace:(NSData *)imgDocumento
                withImagenDocumentoFrente:(NSData *)imgDocumentoFrente
                               withSelfie:(NSData *)imgSelfie
                             withImageKey:(NSString *)imageKey
                              withBarCode:(NSData *)barCode
                      withNumeroDocumento:(NSString *)numeroDocumento
                            withIdProcess:(NSString *)processID
                           withVersionAPI:(NSString *)versionAPI;

-(void)consultaRostroEnrolado:(NSString *) numeroDocumento;

//Procesa la selfie
-(void)procesaFrenteDocumento:(NSData *)nsImFrenteDocumento
              withSelfieImage:(NSData *)nsSelfieImagen
              withNoDocumento:(NSString *)numeroDocumento;

-(void)procesaEnrolamientoRostro:(NSData *)nsSelfieImagen
                 withNoDocumento:(NSString *)numeroDocumento;

-(void)procesaAutenticaRostro:(NSData   *)nsSelfieImagen
              withNoDocumento:(NSString *)numeroDocumento;

-(void)procesaUnEnroll:(NSString *) numeroDocumento;

//Procesa Unicamente el OCR
-(void)procesaOCRDocumento:(NSData *)nsFrontImage
             withBackImage:(NSData *)nsBackImage;

- (void)procesaMatchDocumentoFace:(NSData *)imgSelfie
        withImagenDocumentoFrente:(NSData *)imgDocumentoFrente
                     withImageKey:(NSString *)imageKey
              withNumeroDocumento:(NSString *)numeroDocumento
                    withIdProcess:(NSString *)processID;

/* Realiza el proceso de Enrolado */
- (void)procesaAutenticacionFaceMBSS:(NSData *)imgSelfie
                        withImageKey:(NSString *)imageKey
                 withNumeroDocumento:(NSString *)numeroDocumento
                       withIdProcess:(NSString *)processID;

/* Realiza el Match del rostro con el rostro enrolaso */
- (void)procesaEnrolamientoFaceMBSS:(NSData *)imgSelfie
                       withImageKey:(NSString *)imageKey
                withNumeroDocumento:(NSString *)numeroDocumento
                      withIdProcess:(NSString *)processID;

//Referencia al Delegado
@property (strong, nonatomic)  id <SendDataProcessDelegate> mBiometricDelegate;

@end
