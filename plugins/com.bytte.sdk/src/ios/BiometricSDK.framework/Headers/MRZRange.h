//
//  MRZRange.h
//  BiometricSDK
//
//  Created by RIBEIRO Tiago on 28/04/17.
//  Copyright © 2017 MORPHO. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MRZRange : NSObject

@property (nonatomic, assign) int column;
@property (nonatomic, assign) int columnTo;
@property (nonatomic, assign) int row;


@property (nonatomic, readonly) int length;

- (instancetype)initWithColumn:(int)column columnTo:(int)columnTo andRow:(int)row;

+ (instancetype)rangeWithColumn:(int)column columnTo:(int)columnTo andRow:(int)row;

@end
