//
//  BIONSDictionaryAdapter.h
//  BiometricSDK
//
//  Created by RIBEIRO Tiago on 28/07/2017.
//  Copyright © 2017 MORPHO. All rights reserved.
//

#ifndef BIONSDictionaryAdapter_h
#define BIONSDictionaryAdapter_h

@protocol BIONSDictionaryAdapter <NSObject>

- (NSDictionary * _Nullable)getDictionaryValueWithError:(NSError *__autoreleasing  _Nullable * _Nullable)error;

@end

#endif /* BIONSDictionaryAdapter_h */
