//
//  FaceResponse.h
//  BytteLibrarySDK
//
//  Created by Allan Diaz on 7/31/17.
//  Copyright © 2017 Allan Diaz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import "BaseResponse.h"

@interface FaceResponse : BaseResponse
@property (nonatomic) UIImage  * ImageFaceProceso;
@property (nonatomic) NSString * ImageTemplate;
@property (nonatomic) NSString * ImagePath;
@property (nonatomic) long Score;
@property (nonatomic) bool Autenticado;

-(NSString *) Convert2JSONValue:(NSString *) password;
@end
