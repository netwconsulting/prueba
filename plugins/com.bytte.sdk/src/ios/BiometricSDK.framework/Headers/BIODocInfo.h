//
//  BIODocInfo.h
//  BiometricSDK
//
//  Created by WASAG Kamil - EXT-REPERIO (MORPHO) on 21/11/2016.
//  Copyright © 2016 MORPHO. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef int BIODocInfo;

/**
 *  @author Morpho
 *
 *  @brief BLUR detected
 */
extern const BIODocInfo BIODocInfoBlur;

/**
 *  @author Morpho
 *
 *  @brief Bad framing detected
 */
extern const BIODocInfo BIODocInfoBadFraming;

/**
 *  @author Morpho
 *
 *  @brief Start point and shoot capture
 */
extern const BIODocInfo BIODocInfoStartStill;

/**
 *  @author Morpho
 *
 *  @brief  Point and shoot capture done
 */
extern const BIODocInfo BIODocInfoEndStill;

/**
 *  @author Morpho
 *
 *  @brief Shaking detected
 */
extern const BIODocInfo BIODocInfoShaking;

/**
 *  @author Morpho
 *
 *  @brief Reflection detected
 */
extern const BIODocInfo BIODocInfoReflection;

/**
 *  @author Morpho
 *
 *  @brief Hold document straight
 */
extern const BIODocInfo BIODocInfoHoldStraight;

/**
 *  @author Morpho
 *
 *  @brief Movement of document detected
 */
extern const BIODocInfo BIODocInfoMovementDetected;

/**
 *  @author Morpho
 *
 *  @brief Too far to be detected
 */
extern const BIODocInfo BIODocInfoTooFar;

/**
 *  @author Morpho
 *
 *  @brief Too close to be detected
 */
extern const BIODocInfo BIODocInfoTooClose;

/**
 *  @author Morpho
 *
 *  @brief Everything is OK
 */
extern const BIODocInfo BIODocInfoOK;

NSString * _Nonnull NSStringFromBIODocInfo(BIODocInfo info);
