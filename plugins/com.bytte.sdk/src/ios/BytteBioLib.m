/*
 ||-------------------------------------------------------
 BytteBioLib - Objective C  - Xcode              Allan Diaz :)
 BytteBioLib - Android Java - Android Studio     Venancio Prada
 BytteBioLib - JavaScript   - Visual Studio Code Allan Diaz
 
 //Inicio el Genesis
 Noviembre de 2016
 
 Script con las funciones requeridas para implementar el
 SDK de Bytte Biometrico y Captura codigo de barras desde
 dispositivos IOS Iphone
 Bytte 2016
 
 ||-------------------------------------------------------
 Enero 2017
 Se adiciona la funcionalidad de envio y cotejo del
 lado del servidor
 
 Allan Diaz
 ||-------------------------------------------------------
 Febrero de 2017
 Se corrige la toma del Documento
 Se corrige la ubicacion de la camara para Portrait
 ||-------------------------------------------------------
 Allan Diaz
 ||-------------------------------------------------------
 Julio de 2017
 Se corrige la captura en dispositivos + (Iphone Plus 6 y 7)
 Se corrige el tiempo de captura en dispositivos con procesador A9
 Se homologa la captura de huella(s) al concepto de Android y se remueven los circulos
 ||-------------------------------------------------------
 Septiembre de 2017
 Se Adiciona la Fncionalidad de Rostro y Captura Frente del Documento!
 ||-------------------------------------------------------
 Octubre de 2017
 Se Adiciona la Fncionalidad para ofuscar archivos con clave!!
 Se Adiciona la Funcionalidad para autenticacion de documento sin rostro
 ||-------------------------------------------------------
 Noviembre de 2017
 Se adiciona la validacion de documento (Frente y reverso) con rostro unicamente
 Se adiciona la validacion de Match facial (Rostro vivo Vs Captura) (Basico)
  ||-------------------------------------------------------
 Diciembre de 2017
 Se adiciona el manejo de time Out en las diferentes Capturas
 Se adiciona el retorno de Codigo de Error, en caso de error
 Se adiciona el manejo de Niveles de Captura de rostro (0 a 10)
 Se adiciona el Status de los permisos de la camara (Tiene o no Permisos)
 Se Entrega con la Version BioSmart 4.5.0

 Se Adiciona Lectura del frente  del Documento Cedula Chilena con MRZ y OCR
 Se Adiciona Lectura del reverso del Documento Cedula Chilena con MRZ
 */

#import "BytteBioLib.h"
#import <Cordova/CDV.h>

@implementation BytteBioLib

//Toast
UIAlertController * alertControllerToast;
BioSDKLicence     * obLicencia;

//DataProcess Object
SendDataProcess *oDataProcess;

//Funcion Echo para Test
- (void)echo:(CDVInvokedUrlCommand*)command
{
    CDVPluginResult* pluginResult = nil;
    NSString* msg = [command.arguments objectAtIndex:0];
    
    if (msg == nil || [msg length] == 0) {
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR];
    } else 
    {

        alertControllerToast = [UIAlertController
                                alertControllerWithTitle:@""
                                message:msg
                                preferredStyle:UIAlertControllerStyleAlert];

        // Muestro el controller
       [self.viewController presentViewController:alertControllerToast animated:YES completion:nil];
       
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 3 * NSEC_PER_SEC),
                       dispatch_get_main_queue(), ^{
                            if(alertControllerToast)
                                //Si Hay Alert Controller, Lo finalizo
                                [alertControllerToast dismissViewControllerAnimated:YES completion:nil];
                       });
        
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK
                                         messageAsString:msg];
    }
    
    [self.commandDelegate sendPluginResult:pluginResult
                                callbackId:command.callbackId];
}

//Inicia la captura del Codigo de barras
- (void)startMorphoLicense:(CDVInvokedUrlCommand*)command
{
    //Se obtiene la Orientacion y se forza a Portrait
    [UIApplication sharedApplication].statusBarStyle       = UIStatusBarStyleLightContent;
    [UIApplication sharedApplication].statusBarOrientation = UIInterfaceOrientationPortrait;
    
    //Forzo la Orientacion Portrait
    NSNumber *value = [NSNumber numberWithInt:UIInterfaceOrientationPortrait];
    [[UIDevice currentDevice] setValue:value forKey:@"orientation"];
    
    //Log del Serial enviado
    NSLog(@"startMorphoLicense");

    //Inicializo el Objeto de Licencia
    obLicencia =  [BioSDKLicence new];
    obLicencia.bioSDKLicenceDelegate = self;

    //Licence Manager
    [obLicencia InitLicenceManager];

    // Asigno el pendiente operacion = true
    self.hasPendingOperation = YES;
    
    // Almaceno el comando previo
    self.latestCommand = command;
}

//Delegado de la respuesta del init de la Licencia Morpho
- (void)doBioSDKLicenceDelegate:(LicenseMorphoResponse *)statusLicence
{
    //Espero
    [NSThread sleepForTimeInterval:0.35];
    dispatch_async(dispatch_get_main_queue(),
                   ^{
                       //Espero
                       [NSThread sleepForTimeInterval:0.1];

                       //Numero de Documento
                       NSLog(@"startMorphoLicense Return %@",(statusLicence.licenceStatusOK ? @"OK" : @"Error"));
                       
                       //Convierto a JSON
                       NSString * sJsonRes = [statusLicence Convert2JSONValue];
                       
                       //Retorna
                       [self.commandDelegate sendPluginResult:
                        [CDVPluginResult resultWithStatus:CDVCommandStatus_OK
                                          messageAsString:sJsonRes]
                                                   callbackId:self.latestCommand.callbackId];
                       
                       // Apago la bandera
                       self.hasPendingOperation = NO; 
                   });
}

//Inicia la captura del Frente del Documento
- (void)startFrontDocument:(CDVInvokedUrlCommand*)command
{
    //Se obtiene la Orientacion y se forza a Portrait
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
    [UIApplication sharedApplication].statusBarOrientation = UIInterfaceOrientationPortrait;
    
    //Forzo la Orientacion Portrait
    NSNumber *value = [NSNumber numberWithInt:UIInterfaceOrientationPortrait];
    [[UIDevice currentDevice] setValue:value forKey:@"orientation"];
    
    //Parametro 0
    NSString * msg = [command.arguments objectAtIndex:0];

    //Parametro 1 Password con el que se ofuscarà el archivo!
    NSString * filePWD = [command.arguments objectAtIndex:1];

    //Parametro 2 TimeOut
    NSString * sTimeOut = [command.arguments objectAtIndex:2];
    
    //Key Scandit
    NSString * serialKey = msg;
    
    //Log del Serial enviado
    NSLog(@"MicroBlink Key %@ ",msg);
    
    //Imagen de Frente del Documento a mostrar
    UIImage * imFront = [UIImage imageNamed:@"frontDoc.png"];
    
    //BarCode View Controller
    CameraFrontViewController *vc = [[CameraFrontViewController alloc] initWithKey:serialKey AndUIImageGuide:imFront];
    
    //Suscripcion al evento
    vc.frontDocumentProcessDelegate = self;

    //Asigno el Time Out
    vc.timeOutController      = (short)[sTimeOut intValue];

    //Envio el password
    [vc setFilePassword:filePWD];
    
    // Asigno el pendiente operacion = true
    self.hasPendingOperation = YES;
    
    // Almaceno el comando previo
    self.latestCommand = command;
    
    // Muestro el controller
    [self.viewController presentViewController:vc animated:YES completion:nil];
}

//Inicia la captura del Rostro para Enrolamiento
- (void)startFaceCapture:(CDVInvokedUrlCommand*)command
{
    //Se obtiene la Orientacion y se forza a Portrait
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
    [UIApplication sharedApplication].statusBarOrientation = UIInterfaceOrientationPortrait;

    //Parametro 0 - Minucia para autenticacion!
    NSString * msg = [command.arguments objectAtIndex:0];
    NSLog(@"msg%@ ",msg);

    //Parametro 1 Password con el que se ofuscarà el archivo!
    NSString * filePWD = [command.arguments objectAtIndex:1];

    //Parametro 2 TimeOut
    NSString * sTimeOut = [command.arguments objectAtIndex:2];

    //Parametro 3 FaceCaptureMode
    NSString * sFaceCaptureMode = [command.arguments objectAtIndex:2];
    
    //Forzo la Orientacion Portrait
    NSNumber *value = [NSNumber numberWithInt:UIInterfaceOrientationPortrait];
    [[UIDevice currentDevice] setValue:value forKey:@"orientation"];

    //Obtengo el Bundle de la captura de Rostro
    NSString *bundleIdentifier = @"com.bytte.ios.BytteLibrarySDK";
    NSBundle *bundle = [NSBundle bundleWithIdentifier:bundleIdentifier];
    
    //Obtengo el Story Board
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"SBFaceBoard" bundle:bundle];
    
    //Genero el Controller basado en el Story Board
    BioSDKFaceViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"BioSDKFaceView"];
    vc.bioSDKFaceDelegate        = self;
    
    //Asigno el Time Out
    vc.timeOutController         = (short)[sTimeOut intValue];

    //Face Bio Capture Mode
    vc.faceCaptureMode = (short)[sFaceCaptureMode intValue];

    //Envio el password
    [vc setFilePassword:filePWD];
    
    // Asigno el pendiente operacion = true
    self.hasPendingOperation = YES;
    
    // Almaceno el comando previo
    self.latestCommand = command;
    
    // Muestro el controller
    [self.viewController presentViewController:vc animated:YES completion:nil];
}

//Inicia la Captura de la Huella
-(void) startFingerprint:(CDVInvokedUrlCommand*)command
{
    //Se obtiene la Orientacion y se forza a Portrait
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
    [UIApplication sharedApplication].statusBarOrientation = UIInterfaceOrientationPortrait;
    
    //Forzo la Orientacion Portrait
    NSNumber *value = [NSNumber numberWithInt:UIInterfaceOrientationPortrait];
    [[UIDevice currentDevice] setValue:value forKey:@"orientation"];
    
    //Numero de Dedo (1 al 7)
    NSString* arg0 = [command.arguments objectAtIndex:0];
    NSLog(@"arg0%@ ",arg0);
    
    //Numero de Capturas
    NSString* arg1 = [command.arguments objectAtIndex:1];

    //Parametro 2 Password con el que se ofuscarà el archivo!
    NSString * filePWD = [command.arguments objectAtIndex:2];

    //Parametro 3 TimeOut
    NSString * sTimeOut = [command.arguments objectAtIndex:3];
    
    // Asigno el pendiente operacion = true
    self.hasPendingOperation = YES;
    
    // Almaceno el comando previo
    self.latestCommand = command;

    NSString *bundleIdentifier = @"com.bytte.ios.BytteLibrarySDK";
    NSBundle *bundle           = [NSBundle bundleWithIdentifier:bundleIdentifier];
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"SBFaceBoard" bundle:bundle];

    //Imagenes de las plantillas
    UIImage * imPlantillaDerecha   = [UIImage imageNamed:@"plantilla_mano_derecha.png"];
    UIImage * imPlantillaIzquierda = [UIImage imageNamed:@"plantilla_mano_izquierda.png"];
    
    //Genero el Controller basado en el Story Board
    BioSDKFingerprintViewController *vc; 
    
    //Instancio el ViewController
    vc = [storyboard instantiateViewControllerWithIdentifier:@"BioSDKFingerprintView"];
    vc.fingerprintProcessDelegate = self;
    vc.numeroDedoCaptura          = [arg1 intValue];
    vc.plantillaHuellaDerecha     = imPlantillaDerecha;
    vc.plantillaHuellaIzquierda   = imPlantillaIzquierda;
    
    //Asigno el Time Out
    vc.timeOutController          = (short)[sTimeOut intValue];
     
     //Envio el password
    [vc setFilePassword:filePWD];
    
    // Muestro el controller
    [self.viewController presentViewController:vc animated:YES completion:nil];
}

//Inicia la captura del Codigo de barras
- (void)startBarCode:(CDVInvokedUrlCommand*)command
{
    //Se obtiene la Orientacion y se forza a Portrait
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
    [UIApplication sharedApplication].statusBarOrientation = UIInterfaceOrientationPortrait;
    
    //Forzo la Orientacion Portrait
    NSNumber *value = [NSNumber numberWithInt:UIInterfaceOrientationPortrait];
    [[UIDevice currentDevice] setValue:value forKey:@"orientation"];
    
    //Parametro 0
    NSString * msg     = [command.arguments objectAtIndex:0];

    //Parametro 1 Password con el que se ofuscarà el archivo!
    NSString * filePWD  = [command.arguments objectAtIndex:1];

    //Parametro 2 TimeOut
    NSString * sTimeOut = [command.arguments objectAtIndex:2];
    
    //Key Scandit
    NSString * serialKey = msg;
    
    //Log del Serial enviado
    NSLog(@"BarCode Key %@ ",msg);
    
    //BarCode View Controller
    UIImage * back    = [UIImage imageNamed:@"back_dav_doc.png"];
    UIImage * backDet = [UIImage imageNamed:@"back_dav_doc_det.png"];
    
    //BarCode View Controller
    CameraViewController *vc = [[CameraViewController alloc] initWithKey:serialKey
                                                         AndUIImageGuide:back WithDet:backDet];
    
    //Suscripcion al evento
    vc.capturaBarCodeDelegate = self;

    //Asigno el Time Out
    vc.timeOutController      = (short)[sTimeOut intValue];

    //Envio el password
    [vc setFilePassword:filePWD];
    
    // Asigno el pendiente operacion = true
    self.hasPendingOperation = YES;
    
    // Almaceno el comando previo
    self.latestCommand = command;
    
    // Muestro el controller
    [self.viewController presentViewController:vc animated:YES completion:nil];
}

//--------------------------------------------------------------
//--------------------------------------------------------------
//CallBacks de los Eventos
//--------------------------------------------------------------
//--------------------------------------------------------------

//Respuesta de la Captura del Rostro
- (void)doBioSDKFaceDelegate:(FaceResponse *)faceResponse
                withPassword:(NSString *)filePassword
{
    dispatch_async(dispatch_get_main_queue(),
                   ^{
                       //Convierto a JSON
                       NSString * sJsonRes = [faceResponse Convert2JSONValue:filePassword];

                       //Retorna
                       [self.commandDelegate sendPluginResult:
                        [CDVPluginResult resultWithStatus:CDVCommandStatus_OK
                                          messageAsString:sJsonRes]
                                                   callbackId:self.latestCommand.callbackId];
                       
                       // Apago la bandera
                       self.hasPendingOperation = NO;
                       
                       //Finalizo
                       [self.viewController dismissViewControllerAnimated:YES completion:nil];
                   });
    
}

//Delegado de la captura del frente del documento
- (void)doCapturaFrontDocumentDelegate:(FrontDocumentResponse *)frontResponse
                          withPassword:(NSString *)filePassword
{
    dispatch_async(dispatch_get_main_queue(),
                   ^{                   
                       //Convierto a JSON
                       NSString * sJsonRes = [frontResponse Convert2JSONValue:filePassword];
                       
                       //Retorna
                       [self.commandDelegate sendPluginResult:
                        [CDVPluginResult resultWithStatus:CDVCommandStatus_OK
                                          messageAsString:sJsonRes]
                                                   callbackId:self.latestCommand.callbackId];
                       
                       // Apago la bandera
                       self.hasPendingOperation = NO;
                       
                       //Finalizo
                       [self.viewController dismissViewControllerAnimated:YES completion:nil];
                   });
}

//Delegado de Captura reverso del Documento
- (void)doCapturaBarCode:(InfoDocumento *)infoDocumento 
            withPassword:(NSString *)filePassword
{
    dispatch_async(dispatch_get_main_queue(),
                   ^{                       
                       //Convierto a JSON
                       NSString * sJsonRes = [infoDocumento Convert2JSONValue:filePassword];
                       
                       //Retorna
                       [self.commandDelegate sendPluginResult:
                        [CDVPluginResult resultWithStatus:CDVCommandStatus_OK
                                          messageAsString:sJsonRes]
                                                   callbackId:self.latestCommand.callbackId];
                       
                       // Apago la bandera
                       self.hasPendingOperation = NO;
                       
                       //Finalizo
                       [self.viewController dismissViewControllerAnimated:YES completion:nil];
                   });
}

//Retorno de la captura de huellas Morpho
- (void)doFingerprintProcessDelegate:(FingerprintCaptureResult *)fingerprintCapture
                        withPassword:(NSString *)filePassword
{
    dispatch_async(dispatch_get_main_queue(),
                   ^{
                       //Si hay datos a procesar
                       if (fingerprintCapture)
                       {
                            //Convierto a JSON
                            NSString * sJsonRes  = [fingerprintCapture Convert2JSONValue:filePassword];
                            
                            //Retorna
                            [self.commandDelegate sendPluginResult:
                                [CDVPluginResult resultWithStatus:CDVCommandStatus_OK
                                                messageAsString:sJsonRes]
                                                        callbackId:self.latestCommand.callbackId];
                            
                            
                            // Cambio la bandera!
                            self.hasPendingOperation = NO;
                            
                            //finalizo el controller
                            [self.viewController dismissViewControllerAnimated:YES completion:nil];
                        }
                   });
}

//Delegado con la respuesta de la captura de la huella
- (void)doCapturaFingerprintDelegate:(NSArray *)fingerprintsObjects
{
    dispatch_async(dispatch_get_main_queue(),
                   ^{
                       
                       //Recorro las imagenes
                       for (FingerprintResult * p in fingerprintsObjects)
                       {
                           //Data
                           NSData * data = UIImagePNGRepresentation([p fProcessedImage]);
                           
                           //Genero el Path
                           p.PathBitmap = [Util escribeArchivoLocal:data];
                           p.PathWSQ    = [Util escribeArchivoLocalEx:[p fInvertedMirroredWSQ] withExtension:@"wsq"];
                       }
                       
                       //Convierto a JSON
                       NSString * sJsonRes  = [FingerprintResult Convert2JSONValue:fingerprintsObjects];
                       NSLog(@"sJsonRes: %@", sJsonRes);
                       
                       //Retorna
                       [self.commandDelegate sendPluginResult:
                        [CDVPluginResult resultWithStatus:CDVCommandStatus_OK
                                          messageAsString:sJsonRes]
                                                   callbackId:self.latestCommand.callbackId];
                       
                       
                       // Cambio la bandera!
                       self.hasPendingOperation = NO;
                       
                       //finalizo el controller
                       [self.viewController dismissViewControllerAnimated:YES completion:nil];
                   });
}

//--------------------------------------------------------------
//--------------------------------------------------------------
//--------------------------------------------------------------
//Procesos de envio al Servidor
//Se usan para proposito de DEMO
//--------------------------------------------------------------

//Proceso de Envio de Informacion al Servidor
-(void)sendDataProcess:(CDVInvokedUrlCommand*)command
{
    //ID del Proceso
    NSString* processId = [command.arguments objectAtIndex:0];
    
    //Path Imagen del Documento
    NSString* pathImgDocumento = [command.arguments objectAtIndex:1];
    
    //BarCodeBase64
    NSString* barCodeBase64 = [command.arguments objectAtIndex:2];
    
    //path WSQ Huella 1
    NSString* pathWSQHuella1 = [command.arguments objectAtIndex:3];
    
    //path WSQ Huella 2
    NSString* pathWSQHuella2 = [command.arguments objectAtIndex:4];
    
    //BarCode View Controller
    oDataProcess = [[SendDataProcess alloc] init];
    
    //Me Suscribo al Evento
    oDataProcess.mBiometricDelegate = self;
    
    //Leo el archivo de Imagen Documento
    NSData * oImgDocumento = [Util readFileFromPath:pathImgDocumento];
    
    //Leo el Archivo de WSQ1
    NSData * oWSQ1 = [Util readFileFromPath:pathWSQHuella1];
    
    //Leo el Archivo de WSQ2
    NSData * oWSQ2 = [Util readFileFromPath:pathWSQHuella2];
    
    //Obtengo el Base64
    NSData * oBarCode = [[NSData alloc] initWithBase64EncodedString:barCodeBase64 options:0];
    
    //Envio para Procesamiento
    [oDataProcess procesaBiometriaFingerprint:oImgDocumento
                                  withHuella1:oWSQ1
                                  withHuella2:oWSQ2
                                  withBarCode:oBarCode
                                withIdProcess:processId];
    
    // Asigno el pendiente operacion = true
    self.hasPendingOperation = YES;
    
    // Almaceno el comando previo
    self.latestCommand = command;
}

//Respuesta del Servidor al haber procesado la informacion
- (void)doProcesaFingerprint:(DocumentoInfoResponse *)baseResponse
{
    //Espero
    [NSThread sleepForTimeInterval:0.25];

    dispatch_async(dispatch_get_main_queue(),
                   ^{
                       //Espero
                        [NSThread sleepForTimeInterval:0.85];
                       //Imprimo el Log
                       //NSLog(@"sJsonRes: %@", baseResponse.MensajeOriginal);
                       
                       //Retorna
                       [self.commandDelegate sendPluginResult:
                        [CDVPluginResult resultWithStatus:CDVCommandStatus_OK
                                          messageAsString:baseResponse.MensajeOriginal]
                                                   callbackId:self.latestCommand.callbackId];
                       
                       // Cambio la bandera!
                       self.hasPendingOperation = NO;
                       
                       //Limpio
                       oDataProcess = nil;
                   });
}

//Proceso de Envio de Informacion al Servidor
-(void)sendFingerprintProcess:(CDVInvokedUrlCommand*)command
{
    //ID del Proceso
    NSString* processId = [command.arguments objectAtIndex:0];
    
    //Path Imagen del Documento
    NSString* pathImgDocumento = [command.arguments objectAtIndex:1];
    
    //BarCodeBase64
    NSString* barCodeBase64  = [command.arguments objectAtIndex:2];
    
    //path JPG Huella 1
    NSString* pathImgHuella1 = [command.arguments objectAtIndex:3];
    
    //path JPG Huella 2
    NSString* pathImgHuella2 = [command.arguments objectAtIndex:4];

    //path JPG Huella 3
    NSString* pathImgHuella3 = [command.arguments objectAtIndex:5];
    
    //path JPG Huella 4
    NSString* pathImgHuella4 = [command.arguments objectAtIndex:6];

    //Numero de Documento
    NSString* numeroDocumento = [command.arguments objectAtIndex:7];

    //Logs
    NSLog(@"barCodeBase64 %@ ",barCodeBase64);
    NSLog(@"pathImgHuella1 %@ ",pathImgHuella1);
    NSLog(@"pathImgHuella2 %@ ",pathImgHuella2);
    NSLog(@"pathImgHuella3 %@ ",pathImgHuella3);
    NSLog(@"pathImgHuella4 %@ ",pathImgHuella4);
    
    //BarCode View Controller
    oDataProcess = [[SendDataProcess alloc] init];
    
    //Me Suscribo al Evento
    oDataProcess.mBiometricDelegate = self;
    
    //Leo el archivo de Imagen Documento
    NSData * oImgDocumento = [Util readFileFromPath:pathImgDocumento];
    
    //Leo el Archivo de WSQ1
    NSData * oImg1 = [Util readFileFromPath:pathImgHuella1];
    
    //Leo el Archivo de WSQ2
    NSData * oImg2 = [Util readFileFromPath:pathImgHuella2];
       
    //Leo el Archivo de WSQ3
    NSData * oImg3 = [Util readFileFromPath:pathImgHuella3];

    //Leo el Archivo de WSQ4
    NSData * oImg4 = [Util readFileFromPath:pathImgHuella4];
 
    //Obtengo el Base64
    NSData * oBarCode = [[NSData alloc] initWithBase64EncodedString:barCodeBase64 options:0];

    NSString * vImageKey = @"";
    
    //Envio para Procesamiento
    [oDataProcess procesaBiometriaFingerprintBitmap:oImgDocumento
                                  withHuella1:oImg1
                                  withHuella2:oImg2
                                  withHuella3:oImg3
                                  withHuella4:oImg4
                                 withImageKey:vImageKey
                                  withBarCode:oBarCode
                          withNumeroDocumento:numeroDocumento
                                withIdProcess:processId];
    
    // Asigno el pendiente operacion = true
    self.hasPendingOperation = YES;
    
    // Almaceno el comando previo
    self.latestCommand = command;
}

//Proceso de Envio de Informacion al Servidor
//arg0 :  Numero de Documento o Identificador del Usuario
//arg1 :  Imagen del Rostro  
//arg2 :  Imagen del Documento
-(void)sendFaceProcess:(CDVInvokedUrlCommand*)command
{
    //ID del Proceso
    NSString* numeroDocumento = [command.arguments objectAtIndex:0];
    
    //Path Imagen del Documento
    NSString* pathImgFrenteDocumento = [command.arguments objectAtIndex:1];
    
    //path de la Selfie tomada
    NSString* pathImgSelfie = [command.arguments objectAtIndex:2];

    //Leo el Archivo del Frente del Documento
    NSData * nsFrontImage  = [Util readFileFromPath:pathImgFrenteDocumento];

    //Leo el Archivo de la Selfie
    NSData * nsSelfieImage = [Util readFileFromPath:pathImgSelfie];
    
    //Data Process View Controller
    oDataProcess = [[SendDataProcess alloc] init];
    
    //Me Suscribo al Evento
    oDataProcess.mBiometricDelegate = self;

     //Lanzo el proceso
    dispatch_async(dispatch_get_main_queue(),
    ^{        
        //Realizo la Consulta
        [oDataProcess procesaFrenteDocumento:nsFrontImage
                                withSelfieImage:nsSelfieImage
                                withNoDocumento:numeroDocumento];
    }); 
    
    // Asigno el pendiente operacion = true
    self.hasPendingOperation = YES;
    
    // Almaceno el comando previo
    self.latestCommand = command;
}

//Proceso de Envio de Informacion al Servidor
//arg0 :  Id del Proceso
//arg1 :  Imagen del Frente del Documento
//arg2 :  Imagen del Reverso del Documento
-(void)sendOCRProcess:(CDVInvokedUrlCommand*)command
{
    //ID del Proceso
    NSString* processId = [command.arguments objectAtIndex:0];
    NSLog(@"processId %@ ",processId);
    
    //Path Imagen del Documento
    NSString* pathImgFrenteDocumento = [command.arguments objectAtIndex:1];
    
    //Leo el Archivo del Frente del Documento
    NSData * nsFrontImage  = [Util readFileFromPath:pathImgFrenteDocumento];

    //Path de la Imagen
    NSString * pathImgReversoDocumento = [command.arguments objectAtIndex:2];

    //Leo el archivo
    NSData * nsReversoImage = [Util readFileFromPath:pathImgReversoDocumento];
    
    //Data Process View Controller
    oDataProcess = [[SendDataProcess alloc] init];
    
    //Me Suscribo al Evento
    oDataProcess.mBiometricDelegate = self;

     //Lanzo el proceso
    dispatch_async(dispatch_get_main_queue(),
    ^{        
        //Realizo la Consulta
        [oDataProcess procesaOCRDocumento:nsFrontImage
                            withBackImage:nsReversoImage];
    }); 
    
    // Asigno el pendiente operacion = true
    self.hasPendingOperation = YES;
    
    // Almaceno el comando previo
    self.latestCommand = command;
}

//Respuesta del Delegado del Server para la informacion OCR!
- (void)doProcesaOCRResponse:(OCRResponse *)OCRInfo
{
    dispatch_async(dispatch_get_main_queue(),
                   ^{

                    //Espero
                    [NSThread sleepForTimeInterval:0.2];
                    //Imprimo el Log
                    //NSLog(@"sJsonRes: %@", baseResponse.MensajeOriginal);
                     
                    //Retorna
                    [self.commandDelegate sendPluginResult:
                    [CDVPluginResult resultWithStatus:CDVCommandStatus_OK
                                        messageAsString:OCRInfo.MensajeOriginal]
                                                callbackId:self.latestCommand.callbackId];

                    // Cambio la bandera!
                    self.hasPendingOperation = NO;

                    //Limpio
                    oDataProcess = nil;
                   });
}

//Proceso de Envio de Informacion al Servidor
//Para Autenticacion del Documento
//arg0  :  Id del Proceso
//arg1  :  NumeroDocumento
//arg2  :  Imagen del Frente del Documento
//arg3  :  Imagen del Reverso del Documento
//arg4  :  BarCodeBase64
//arg5  :  Huella #1
//arg6  :  Huella #2
//arg7  :  Huella #3
//arg8  :  Huella #4
//arg9  :  FilePwd -> Password con que se ofuscaron las imagenes
//arg10 :  Version -> 2
-(void)sendAutenticacionDocumentoProcess:(CDVInvokedUrlCommand*)command
{
    //ID del Proceso
    NSString* processId = [command.arguments objectAtIndex:0];
    NSLog(@"processId %@ ",processId);

    //Parametro 1 Numero Documento en claro
    NSString * numeroDocumento = [command.arguments objectAtIndex:1];
    
    //Path Imagen del frente del Documento
    NSString* pathImgFrenteDocumento = [command.arguments objectAtIndex:2];
    
    //Leo el Archivo del Frente del Documento
    NSData * nsFrontImage  = [Util readFileFromPath:pathImgFrenteDocumento];

    //Path de la Imagen reversi del documento
    NSString * pathImgReversoDocumento = [command.arguments objectAtIndex:3];

    //Leo el archivo
    NSData * nsReversoImage = [Util readFileFromPath:pathImgReversoDocumento];
        
    //BarCodeBase64
    NSString* barCodeBase64  = [command.arguments objectAtIndex:4];
    NSData  * oBarCode       = [[NSData alloc] initWithBase64EncodedString:barCodeBase64 options:0];
    
    //path WSQ Huella 1
    NSString* pathImgHuella1 = [command.arguments objectAtIndex:5];
    NSData  * nsDataHuella1  = [Util readFileFromPath:pathImgHuella1];
    
    //path WSQ Huella 2
    NSString* pathImgHuella2 = [command.arguments objectAtIndex:6];
    NSData  * nsDataHuella2  = [Util readFileFromPath:pathImgHuella2];

    //path WSQ Huella 3
    NSString* pathImgHuella3 = [command.arguments objectAtIndex:7];
    NSData  * nsDataHuella3  = [Util readFileFromPath:pathImgHuella3];
    
    //path WSQ Huella 4
    NSString* pathImgHuella4 = [command.arguments objectAtIndex:8];
    NSData  * nsDataHuella4  = [Util readFileFromPath:pathImgHuella4];

    //Parametro 9 Password con el que estan ofuscadas las imagenes
    NSString * filePWD = [command.arguments objectAtIndex:9];

    //Data Process View Controller
    oDataProcess = [[SendDataProcess alloc] init];
    
    //Me Suscribo al Evento
    oDataProcess.mBiometricDelegate = self;

     //Lanzo el proceso
    dispatch_async(dispatch_get_main_queue(),
    ^{        
        //Proceso en el Server
        [oDataProcess procesaAutenticacionDocumento:nsReversoImage
                            withImagenDocumentoFrente:nsFrontImage
                                            withHuella1:nsDataHuella1
                                            withHuella2:nsDataHuella2
                                            withHuella3:nsDataHuella3
                                            withHuella4:nsDataHuella4
                                            withImageKey:filePWD
                                            withBarCode:oBarCode
                                    withNumeroDocumento:numeroDocumento
                                        withIdProcess:processId];
    }); 
    
    // Asigno el pendiente operacion = true
    self.hasPendingOperation = YES;
    
    // Almaceno el comando previo
    self.latestCommand = command;
}

//Callback del proceso de Autenticacion
- (void)doProcesaAutenticacionDocumento:(AutenticacionDocumentoResponse *)AutenticacionInfo
{
    dispatch_async(dispatch_get_main_queue(),
                   ^{

                    //Espero
                    [NSThread sleepForTimeInterval:0.2];
                    //Imprimo el Log
                    //NSLog(@"sJsonRes: %@", baseResponse.MensajeOriginal);
                     
                    //Retorna
                    [self.commandDelegate sendPluginResult:
                    [CDVPluginResult resultWithStatus:CDVCommandStatus_OK
                                        messageAsString:AutenticacionInfo.MensajeOriginal]
                                                callbackId:self.latestCommand.callbackId];

                    // Cambio la bandera!
                    self.hasPendingOperation = NO;

                    //Limpio
                    oDataProcess = nil;
                   });
}

//--------------------------------------------------------------------------------------

//Match Facial unicamente
//Proceso de Envio de Informacion al Servidor
//Para Match facial del Documento Vs Rostro del usuario
//arg0  :  Id del Proceso
//arg1  :  NumeroDocumento
//arg2  :  Imagen del Frente del Documento
//arg3  :  Path Imagen Selfie Usuario
//arg4  :  FilePwd -> Password con que se ofuscaron las imagenes
//arg5  :  Version -> 1
-(void)sendMatchDocumentoFaceProcess:(CDVInvokedUrlCommand*)command
{
    //ID del Proceso
    NSString* processId = [command.arguments objectAtIndex:0];
    NSLog(@"processId %@ ",processId);

    //Parametro 1 Numero Documento en claro
    NSString * numeroDocumento = [command.arguments objectAtIndex:1];
    
    //Path Imagen del frente del Documento
    NSString* pathImgFrenteDocumento = [command.arguments objectAtIndex:2];
    
    //Leo el Archivo del Frente del Documento
    NSData * nsFrontImage  = [Util readFileFromPath:pathImgFrenteDocumento];

    //Path de la Imagen selfie
    NSString * pathImgSelfie = [command.arguments objectAtIndex:3];

    //Leo el archivo
    NSData * nsSelfieImage = [Util readFileFromPath:pathImgSelfie];
        
    //Parametro con el que estan ofuscadas las imagenes
    NSString * filePWD = [command.arguments objectAtIndex:4];

    //Parametro APi Version
    NSString * apiVersion = @"MatchDocumento";

    //Data Process View Controller
    oDataProcess = [[SendDataProcess alloc] init];
    
    //Me Suscribo al Evento
    oDataProcess.mBiometricDelegate = self;

     //Lanzo el proceso
    dispatch_async(dispatch_get_main_queue(),
    ^{        
        //Proceso en el Server
        [oDataProcess procesaMatchDocumentoFace:nsSelfieImage
                            withImagenDocumentoFrente:nsFrontImage
                                            withImageKey:filePWD
                                    withNumeroDocumento:numeroDocumento
                                        withIdProcess:processId
                                        withVersionAPI:apiVersion];
    }); 
    
    // Asigno el pendiente operacion = true
    self.hasPendingOperation = YES;
    
    // Almaceno el comando previo
    self.latestCommand = command;
}


//Proceso de Envio de Informacion al Servidor
//Para Autenticacion del Documento Face
//arg0  :  Id del Proceso
//arg1  :  NumeroDocumento
//arg2  :  Imagen del Frente del Documento
//arg3  :  Imagen del Reverso del Documento
//arg4  :  BarCodeBase64
//arg5  :  Path de la Selfie del Usuario
//arg6  :  FilePwd -> Password con que se ofuscaron las imagenes
//arg7  :  Version -> 1
-(void)sendAutenticacionDocumentoFaceProcess:(CDVInvokedUrlCommand*)command
{
    //ID del Proceso
    NSString* processId = [command.arguments objectAtIndex:0];
    NSLog(@"processId %@ ",processId);

    //Parametro 1 Numero Documento en claro
    NSString * numeroDocumento = [command.arguments objectAtIndex:1];
    
    //Path Imagen del frente del Documento
    NSString* pathImgFrenteDocumento = [command.arguments objectAtIndex:2];
    
    //Leo el Archivo del Frente del Documento
    NSData * nsFrontImage  = [Util readFileFromPath:pathImgFrenteDocumento];

    //Path de la Imagen reversi del documento
    NSString * pathImgReversoDocumento = [command.arguments objectAtIndex:3];

    //Leo el archivo
    NSData * nsReversoImage = [Util readFileFromPath:pathImgReversoDocumento];
        
    //BarCodeBase64
    NSString* barCodeBase64  = [command.arguments objectAtIndex:4];
    NSData  * oBarCode       = [[NSData alloc] initWithBase64EncodedString:barCodeBase64 options:0];
    
    //path Imagen Selfie
    NSString* pathSelfieImage = [command.arguments objectAtIndex:5];
    NSData  * nsSelfieImage  = [Util readFileFromPath:pathSelfieImage];
    
    //Parametro 6 Password con el que estan ofuscadas las imagenes
    NSString * filePWD = [command.arguments objectAtIndex:6];

    //Parametro 7 APi Version
    NSString * apiVersion = [command.arguments objectAtIndex:7];

    //Data Process View Controller
    oDataProcess = [[SendDataProcess alloc] init];
    
    //Me Suscribo al Evento
    oDataProcess.mBiometricDelegate = self;

     //Lanzo el proceso
    dispatch_async(dispatch_get_main_queue(),
    ^{        
        //Proceso en el Server
        [oDataProcess procesaAutenticacionDocumentoFace:nsReversoImage
                            withImagenDocumentoFrente:nsFrontImage
                                            withSelfie:nsSelfieImage
                                            withImageKey:filePWD
                                            withBarCode:oBarCode
                                    withNumeroDocumento:numeroDocumento
                                        withIdProcess:processId
                                       withVersionAPI:apiVersion];
    }); 
    
    // Asigno el pendiente operacion = true
    self.hasPendingOperation = YES;
    
    // Almaceno el comando previo
    self.latestCommand = command;
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
//Parametros del Documento Chileno
//Inicia la captura del Frente del Documento Chileno
- (void)startChileFrontDocument:(CDVInvokedUrlCommand*)command
{
    //Se obtiene la Orientacion y se forza a Portrait
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
    [UIApplication sharedApplication].statusBarOrientation = UIInterfaceOrientationPortrait;
    
    //Forzo la Orientacion Portrait
    NSNumber *value = [NSNumber numberWithInt:UIInterfaceOrientationPortrait];
    [[UIDevice currentDevice] setValue:value forKey:@"orientation"];
    
    //Parametro 0
    NSString * msg = [command.arguments objectAtIndex:0];
    
    //Key Scandit
    NSString * serialKey = msg;
    
    //Log del Serial enviado
    NSLog(@"MicroBlink Key %@ ",msg);
    
    //Imagen de Frente del Documento a mostrar
    UIImage * imFront = [UIImage imageNamed:@"frontDocChile.png"];
    
    //Chile Front View Controller
    ChileFrontViewController *vc = [[ChileFrontViewController alloc] 
                                    initWithKey:serialKey 
                                AndUIImageGuide:imFront];
    
    //Suscripcion al evento
    vc.frontChileDocumentProcessDelegate = self;

    // Asigno el pendiente operacion = true
    self.hasPendingOperation = YES;
    
    // Almaceno el comando previo
    self.latestCommand = command;
    
    // Muestro el controller
    [self.viewController presentViewController:vc animated:YES completion:nil];
}

//Inicia la captura del Reverso del Documento Chileno
//Contiene el Código MRZ para saber la ubicacion del documento
- (void)startChileReversoMRZDocument:(CDVInvokedUrlCommand*)command
{
    //Se obtiene la Orientacion y se forza a Portrait
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
    [UIApplication sharedApplication].statusBarOrientation = UIInterfaceOrientationPortrait;
    
    //Forzo la Orientacion Portrait
    NSNumber *value = [NSNumber numberWithInt:UIInterfaceOrientationPortrait];
    [[UIDevice currentDevice] setValue:value forKey:@"orientation"];
    
    //Parametro 0
    NSString * msg = [command.arguments objectAtIndex:0];
    
    //Key Scandit
    NSString * serialKey = msg;
    
    //Log del Serial enviado
    NSLog(@"MicroBlink Key %@ ",msg);
    
    //Imagen Guia del Back
    UIImage * imFront = [UIImage imageNamed:@"back_chile_doc.png"];
    UIImage * backDet = [UIImage imageNamed:@"back_chile_doc_det.png"];
    
    //BarCode View Controller
    ChileBackViewController *vc = [[ChileBackViewController alloc] initWithKey:serialKey AndUIImageGuide:imFront WithDet:backDet];
    
    //Suscripcion al evento
    vc.capturaMRZChileCodeDelegate = self;

    // Asigno el pendiente operacion = true
    self.hasPendingOperation = YES;
    
    // Almaceno el comando previo
    self.latestCommand = command;
    
    // Muestro el controller
    [self.viewController presentViewController:vc animated:YES completion:nil];
}

//Retorno de la Captura de la parte reversa del Documento
- (void)doCapturaMRZChileCodeDelegate:(InfoChileDocumento *)infoDocumento
{
    dispatch_async(dispatch_get_main_queue(),
                   ^{
                       //Convierto a JSON
                       NSString * sJsonRes = [infoDocumento Convert2JSONValue:@""];
                       
                       //Retorna
                       [self.commandDelegate sendPluginResult:
                        [CDVPluginResult resultWithStatus:CDVCommandStatus_OK
                                          messageAsString:sJsonRes]
                                                   callbackId:self.latestCommand.callbackId];
                       
                       // Apago la bandera
                       self.hasPendingOperation = NO;
                       
                       //Finalizo
                       [self.viewController dismissViewControllerAnimated:YES completion:nil];
                   });
}

//Retorno del delegado del frente documento chileno
- (void)doCapturaChileFrontDocumentDelegate:(FrontDocumentResponse *)frontResponse
{
    dispatch_async(dispatch_get_main_queue(),
                   ^{
                       //Convierto a JSON
                       NSString * sJsonRes = [frontResponse Convert2JSONValue:@""];
                       
                       //Retorna
                       [self.commandDelegate sendPluginResult:
                        [CDVPluginResult resultWithStatus:CDVCommandStatus_OK
                                          messageAsString:sJsonRes]
                                                   callbackId:self.latestCommand.callbackId];
                       
                       // Apago la bandera
                       self.hasPendingOperation = NO;
                       
                       //Finalizo
                       [self.viewController dismissViewControllerAnimated:YES completion:nil];
                   });
}


//Envio al Servidor
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//Match Facial unicamente
//Proceso de Envio de Informacion al Servidor
//Para Match facial del Documento Vs Rostro del usuario
//arg0  :  Id del Proceso
//arg1  :  NumeroDocumento
//arg2  :  Imagen del Frente del Documento
//arg3  :  Path Imagen Selfie Usuario
//arg4  :  FilePwd -> Password con que se ofuscaron las imagenes -> ''
-(void)sendMatchDocumentoFaceProcessBase:(CDVInvokedUrlCommand*)command
{
    //ID del Proceso
    NSString* processId = [command.arguments objectAtIndex:0];
    NSLog(@"processId %@ ",processId);
    
    //Parametro 1 Numero Documento en claro
    NSString * numeroDocumento = [command.arguments objectAtIndex:1];
    
    //Path Imagen del frente del Documento
    NSString* pathImgFrenteDocumento = [command.arguments objectAtIndex:2];
    
    //Leo el Archivo del Frente del Documento
    NSData * nsFrontImage  = [Util readFileFromPath:pathImgFrenteDocumento];
    
    //Path de la Imagen selfie
    NSString * pathImgSelfie = [command.arguments objectAtIndex:3];
    
    //Leo el archivo
    NSData * nsSelfieImage = [Util readFileFromPath:pathImgSelfie];
    
    //Parametro con el que estan ofuscadas las imagenes
    NSString * filePWD = [command.arguments objectAtIndex:4];
    
    //Data Process View Controller
    oDataProcess = [[SendDataProcess alloc] init];
    
    //Me Suscribo al Evento
    oDataProcess.mBiometricDelegate = self;
    
    //Lanzo el proceso
    dispatch_async(dispatch_get_main_queue(),
                   ^{
                       //Realizo la Consulta
                       [oDataProcess procesaMatchDocumentoFace:nsSelfieImage
                                     withImagenDocumentoFrente:nsFrontImage
                                                  withImageKey:filePWD
                                           withNumeroDocumento:numeroDocumento
                                                 withIdProcess:processId];
                   });
    
    // Asigno el pendiente operacion = true
    self.hasPendingOperation = YES;
    
    // Almaceno el comando previo
    self.latestCommand = command;
}

//Delegado del Match Documento
- (void)doMatchDocumento:(MBSSProcessResponse *)baseResponse
{
    dispatch_async(dispatch_get_main_queue(),
        ^{
            //Convierto a JSON
            NSString * sJsonRes = [baseResponse Convert2JSONValue];
            
            //Retorna
            [self.commandDelegate sendPluginResult:
            [CDVPluginResult resultWithStatus:CDVCommandStatus_OK
                                messageAsString:sJsonRes]
                                        callbackId:self.latestCommand.callbackId];
            
            // Apago la bandera
            self.hasPendingOperation = NO;
            
            //Finalizo
            [self.viewController dismissViewControllerAnimated:YES completion:nil];
        });
}

//Match Facial unicamente
//Proceso de Envio de Informacion al Servidor
//Consulta si un usuario está o no enrolado
//arg0  :  NumeroDocumento
-(void)sendConsultaUsuarioEnrolado:(CDVInvokedUrlCommand*)command
{
    //Parametro 0 Numero Documento en claro
    NSString * numeroDocumento = [command.arguments objectAtIndex:0];
    
    //Data Process View Controller
    oDataProcess = [[SendDataProcess alloc] init];
    
    //Me Suscribo al Evento
    oDataProcess.mBiometricDelegate = self;
    
    //Lanzo el proceso
    dispatch_async(dispatch_get_main_queue(),
        ^{
            //Realizo la Consulta
            [oDataProcess consultaRostroEnrolado:numeroDocumento];
        });
    
    // Asigno el pendiente operacion = true
    self.hasPendingOperation = YES;
    
    // Almaceno el comando previo
    self.latestCommand = command;
}

//Respuesta del Usuario Enrolado
- (void)doConsultaUsuario:(MBSSUserResponse *)baseResponse
{
    dispatch_async(dispatch_get_main_queue(),
        ^{
            //Convierto a JSON
            NSString * sJsonRes = [baseResponse Convert2JSONValue];

            //Retorna
            [self.commandDelegate sendPluginResult:
            [CDVPluginResult resultWithStatus:CDVCommandStatus_OK
                                messageAsString:sJsonRes]
                                    callbackId:self.latestCommand.callbackId];

            // Apago la bandera
            self.hasPendingOperation = NO;

            //Finalizo
            [self.viewController dismissViewControllerAnimated:YES completion:nil];
        });
}

//Enrolamiento Facial unicamente MBSS
//Proceso de Envio de Informacion al Servidor para enrolamiento
//Para Match facial del Documento Vs Rostro del usuario
//arg0  :  Id del Proceso
//arg1  :  NumeroDocumento
//arg2  :  Path Imagen Selfie Usuario
//arg3  :  FilePwd -> Password con que se ofuscaron las imagenes -> ''
-(void)sendEnrolamientoFaceMBSS:(CDVInvokedUrlCommand*)command
{
    //ID del Proceso
    NSString* processId = [command.arguments objectAtIndex:0];
    NSLog(@"processId %@ ",processId);
    
    //Parametro 1 Numero Documento en claro
    NSString * numeroDocumento = [command.arguments objectAtIndex:1];
        
    //Path de la Imagen selfie
    NSString * pathImgSelfie = [command.arguments objectAtIndex:2];
    
    //Leo el archivo
    NSData * nsSelfieImage = [Util readFileFromPath:pathImgSelfie];
    
    //Parametro con el que estan ofuscadas las imagenes
    NSString * filePWD = [command.arguments objectAtIndex:3];
    
    //Data Process View Controller
    oDataProcess = [[SendDataProcess alloc] init];
    
    //Me Suscribo al Evento
    oDataProcess.mBiometricDelegate = self;
    
    //Lanzo el proceso
    dispatch_async(dispatch_get_main_queue(),
        ^{
            //Realizo el proceso de Enrolamiento
            [oDataProcess procesaEnrolamientoFaceMBSS:nsSelfieImage
                                     withImageKey:filePWD
                              withNumeroDocumento:numeroDocumento
                                    withIdProcess:processId];
        });
    
    // Asigno el pendiente operacion = true
    self.hasPendingOperation = YES;
    
    // Almaceno el comando previo
    self.latestCommand = command;
}

//Delegado del Match
- (void)doEnrolaFaceMBSS:(MBSSProcessResponse *)baseResponse
{
    //Lanzo el proceso
    dispatch_async(dispatch_get_main_queue(),
        ^{

            //Convierto a JSON
            NSString * sJsonRes = [baseResponse Convert2JSONValue];

            //Retorna
            [self.commandDelegate sendPluginResult:
            [CDVPluginResult resultWithStatus:CDVCommandStatus_OK
                                messageAsString:sJsonRes]
                                    callbackId:self.latestCommand.callbackId];

            // Apago la bandera
            self.hasPendingOperation = NO;

            //Finalizo
            [self.viewController dismissViewControllerAnimated:YES completion:nil];

                       
        });
}

//Autenticacion Facial unicamente MBSS
//Proceso de Envio de Informacion al Servidor para enrolamiento
//Para Match facial del Documento Vs Rostro del usuario
//arg0  :  Id del Proceso
//arg1  :  NumeroDocumento
//arg2  :  Path Imagen Selfie Usuario
//arg3  :  FilePwd -> Password con que se ofuscaron las imagenes -> ''
-(void)sendAutenticacionFaceMBSS:(CDVInvokedUrlCommand*)command
{
    //ID del Proceso
    NSString* processId = [command.arguments objectAtIndex:0];
    NSLog(@"processId %@ ",processId);
    
    //Parametro 1 Numero Documento en claro
    NSString * numeroDocumento = [command.arguments objectAtIndex:1];
        
    //Path de la Imagen selfie
    NSString * pathImgSelfie = [command.arguments objectAtIndex:2];
    
    //Leo el archivo
    NSData * nsSelfieImage = [Util readFileFromPath:pathImgSelfie];
    
    //Parametro con el que estan ofuscadas las imagenes
    NSString * filePWD = [command.arguments objectAtIndex:3];
    
    //Data Process View Controller
    oDataProcess = [[SendDataProcess alloc] init];
    
    //Me Suscribo al Evento
    oDataProcess.mBiometricDelegate = self;
    
    //Lanzo el proceso
    dispatch_async(dispatch_get_main_queue(),
        ^{
            //Realizo el proceso de Enrolamiento
            [oDataProcess procesaAutenticacionFaceMBSS:nsSelfieImage
                                     withImageKey:filePWD
                              withNumeroDocumento:numeroDocumento
                                    withIdProcess:processId];
        });
    
    // Asigno el pendiente operacion = true
    self.hasPendingOperation = YES;
    
    // Almaceno el comando previo
    self.latestCommand = command;
}

//Delegado del Match
- (void)doAutenticaFaceMBSS:(MBSSProcessResponse *)baseResponse
{
    //Lanzo el proceso
    dispatch_async(dispatch_get_main_queue(),
        ^{

            //Convierto a JSON
            NSString * sJsonRes = [baseResponse Convert2JSONValue];

            //Retorna
            [self.commandDelegate sendPluginResult:
            [CDVPluginResult resultWithStatus:CDVCommandStatus_OK
                                messageAsString:sJsonRes]
                                    callbackId:self.latestCommand.callbackId];

            // Apago la bandera
            self.hasPendingOperation = NO;

            //Finalizo
            [self.viewController dismissViewControllerAnimated:YES completion:nil];
            
        });
}

@end