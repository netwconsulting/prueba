//
//  BIOSDK.h
//  BiometricSDK
//
//  Created by WASAG Kamil - EXT-REPERIO (MORPHO) on 28/04/2016.
//  Copyright © 2016 WASAG Kamil - EXT-REPERIO (MORPHO). All rights reserved.
//

#import <Foundation/Foundation.h>
#import <BiometricSDK/BIOLicenseOptions.h>
#import <BiometricSDK/BIOLicense.h>
#import <BiometricSDK/IFaceCaptureHandler.h>
#import <BiometricSDK/FaceCaptureOptions.h>
#import <BiometricSDK/IFingerCaptureHandler.h>
#import <BiometricSDK/FingerCaptureOptions.h>
#import <BiometricSDK/IBIOMatcherHandler.h>
#import <BiometricSDK/DocumentCaptureOptions.h>
#import <BiometricSDK/IDocumentCaptureHandler.h>
#import <BiometricSDK/BIOSDKInfo.h>
#import <BiometricSDK/BIOMatcherHandlerOptions.h>




/**
 * @class: BIOSDK
 *
 * @brief This calss is entrypoint to create handlers thats operate on Biometrics
 *
 * @description There is 4 type of object to get and process data:
 *
 *              - FaceCaptureHandler: capture face\n
 *
 *              - FingerCaptureHandler: capture fingers\n
 *
 *              - DocumentCaptureHandler: capture information form MRZ on document and can take a photo of document\n
 *
 *              - BIOMatcherHandler: object used to identify or verify person\n
 *
 *              Object also return basic information about BiometricSDK for e.g. version number.
 *
 */

@interface BIOSDK : NSObject


/**
 *  Method that creates and return FaceCaptureHandler object. This method is asynchronous.
 *
 *  @param options           Settings that will be applaied to FaceCaptureHandler
 *  @param completionHandler Block of code that will be excecuted when method end works. It will return id<FaceCaptureHandler> in case of success otherwise NSError
 */
+ (void)createFaceCaptureHandlerWithOptions:(FaceCaptureOptions * _Nonnull )options withCompletionHandler:(void  (^ _Nonnull )(id<FaceCaptureHandler> _Nullable captureHandler, NSError* _Nullable  error))completionHandler;


/**
 *  Method that creates and return FingerCaptureHandler object. This method is asynchronous.
 *
 *  @param options           Settings that will be applaied to FingerCaptureHandler
 *  @param completionHandler Block of code that will be excecuted when method end works. It will return id<FingerCaptureHandler> in case of success otherwise NSError
 */
+ (void)createFingerCaptureHandlerWithOptions:(FingerCaptureOptions * _Nonnull)options withCompletionHandler:(void (^ _Nonnull)(id<FingerCaptureHandler> _Nullable captureHandler, NSError * _Nullable error))completionHandler;


/**
 *  Method that creates and return BIOMatcherHandler object. This method is asynchronous.
 *
 *  @param options           Settings that will be applied to BIOMatcherHandler
 *  @param completionHandler Block of code that will be executed when method end works. It will return id<BIOMatcherHandler> i case of success otherwise NSError
 */
+ (void)createMatcherHandlerWithOptions:(BIOMatcherHandlerOptions  * _Nonnull )options withCompletionHandler:(void(^ _Nonnull)(id<BIOMatcherHandler> _Nullable matcherHandler, NSError* _Nullable error))completionHandler;


/**
 *  Method that creates and return DocumentCaptureHandler object. This method is asynchronous.
 *
 *  @param options           Setting that will be applied to DocumentCaptureHandler
 *  @param completionHandler Block of coe that will be executed when method end works.
 */
+ (void)createDocumentCaptureHandlerWithOptions:(DocumentCaptureOptions * _Nonnull)options withCompletionHandler:(void(^ _Nonnull)(id<DocumentCaptureHandler> _Nullable handler, NSError* _Nullable error))completionHandler;
+ (void)createDocumentCaptureHandler:(DocumentCaptureOptions * _Nonnull)options withCompletionHandler:(void(^ _Nonnull)(id<DocumentCaptureHandler> _Nullable handler, NSError* _Nullable error))completionHandler __deprecated_msg("Use `createDocumentCaptureHandlerWithOptions:withCompletionHandler:` instead");

/**
 *  Return object that contain information about current version of BiometricSDK e.g. version number
 *
 *  @return Object thay contains information about SDK
 */
+ (BIOSDKInfo* _Nonnull)getInfo;


@end
