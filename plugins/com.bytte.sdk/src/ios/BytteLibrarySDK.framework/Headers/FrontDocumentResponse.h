//
//  FrontDocumentResponse.h
//  BytteLibrarySDK
//
//  Created by Allan Diaz on 8/9/17.
//  Copyright © 2017 Allan Diaz. All rights reserved.
//

#import "BaseResponse.h"
#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>


@interface FrontDocumentResponse : BaseResponse
@property (nonatomic) BOOL EsCredencial;
@property (nonatomic) UIImage  * ImagenFrenteDocumento;
@property (nonatomic) NSString * PathImagen;

-(NSString *) Convert2JSONValue:(NSString *) password;
@end
