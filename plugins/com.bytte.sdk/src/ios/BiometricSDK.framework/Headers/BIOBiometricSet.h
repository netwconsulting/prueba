//
//  BIOBiometricSet.h
//  BiometricSDK
//
//  Created by RIBEIRO Tiago on 10/04/17.
//  Copyright © 2017 MORPHO. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <BiometricSDK/BIOTemplate.h>
#import <BiometricSDK/BIOModality.h>

@interface BIOBiometricSet : NSObject

@property (nonatomic, readonly, nonnull) NSArray<BIOTemplate *> *templates;

- (NSError * __nullable)addTemplate:(BIOTemplate * __nonnull)template;
- (NSError * __nullable)addTemplates:(NSArray<BIOTemplate *> * __nonnull)templates;

@end
