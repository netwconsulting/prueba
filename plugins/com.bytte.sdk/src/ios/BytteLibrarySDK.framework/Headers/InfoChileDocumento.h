//
//  InfoChileDocumento.h
//  BytteLibrarySDK
//
//  Created by Allan Diaz on 11/28/17.
//  Copyright © 2017 Allan Diaz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import "BaseResponse.h"

@interface InfoChileDocumento : BaseResponse

@property (nonatomic) NSString * TipoDocumentoString;
@property (nonatomic) NSNumber * TipoDocumento;
@property (nonatomic) NSString * Emisor;
@property (nonatomic) NSString * NumeroDocumento;
@property (nonatomic) NSString * CodigoDocumento;
@property (nonatomic) NSString * FechaExpiracionString;
@property (nonatomic) NSDate   * FechaExpiracion;
@property (nonatomic) NSString * Apellidos;
@property (nonatomic) NSString * Nombres;
@property (nonatomic) NSString * NombresCompletos;
@property (nonatomic) NSString * FechaNacimientoString;
@property (nonatomic) NSDate   * FechaNacimiento;
@property (nonatomic) NSString * Nacionalidad;
@property (nonatomic) NSString * Sexo;
@property (nonatomic) NSString * RUN;
@property (nonatomic) NSString * OPT;
@property (nonatomic) NSString * MRZ;
@property             NSString * pathImagen;
@property             UIImage  * ImageSample;
@property             NSData   * ImageData;

//Convierte el Objeto a JSON
-(NSString *) Convert2JSONValue:(NSString *) password;
@end

