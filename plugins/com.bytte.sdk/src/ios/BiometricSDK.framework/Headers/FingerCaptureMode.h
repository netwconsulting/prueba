//
//  FingerprintCaptureMode.m
//  BiometricSDK
//
//  Created by RIBEIRO Tiago on 15/03/17.
//  Copyright © 2017 MORPHO. All rights reserved.
//


typedef NS_ENUM(NSUInteger, FingerCaptureMode) {
    
    
    /**
     *   Sets FingerCaptureHandler to detect left hand
     */
    FingerCaptureModeLeftHand,
    
    /**
     *   Sets FingerCaptureHandler to detect right hand
     */
    FingerCaptureModeRightHand,
    
    FingerCaptureModeLeftHandAuthentication,
    
    FingerCaptureModeRightHandAuthentication,
    
};

NSString * NSStringFromFingerCaptureMode(FingerCaptureMode fingerCaptureMode);
