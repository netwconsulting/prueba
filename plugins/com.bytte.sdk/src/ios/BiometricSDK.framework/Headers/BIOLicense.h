//
//  BIOLicense.h
//  BiometricSDK
//
//  Created by WASAG Kamil - EXT-REPERIO (MORPHO) on 28/04/2016.
//  Copyright © 2016 WASAG Kamil - EXT-REPERIO (MORPHO). All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LKMSLicense.h"

/**
 *  Object that contians information about License: ID, binary data and activated features.
 */

@interface BIOLicense : LKMSLicense

@end
