//
//  ServerProcess.h
//  BytteLibrarySDKTest
//
//  Created by Allan Diaz on 1/5/17.
//  Copyright © 2017 Allan Diaz. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseResponse.h"


/*Local
 #define URL_WS_BASE  @"http://192.168.1.13/CASB.ProcesoAutenticacionRest/api"
 #define AMBIENTE_TIPO @"Des"
*/

/*Laboratorio */
#define URL_WS_BASE @"https://portal.bytte.com.co/casb/ProcesoAutenticacion/V4.0/CASB.ProcesoAutenticacionRest/API"
#define AMBIENTE_TIPO @"Lab"

 
/* Produccion
 #define URL_WS_BASE @"http://192.168.1.13/CASB.ProcesoAutenticacionRest/api"
 #define AMBIENTE_TIPO @"Prod"
*/

//URL Rest Multibiometrico Laboratoro
#define URL_WS_MBSS_BASE @"https://portal.bytte.com.co/casb/ProcesoAutenticacion/V4.0/CASB.ProcesoAutenticacionRest/API"

//URL Rest Multibiometrico Desarrollo
//#define URL_WS_MBSS_BASE @"http://192.168.1.13/CASB.ProcesoAutenticacionRest/api"


//Define el delegado para manejo de eventos
@protocol ProcesaServerDelegate <NSObject>
@optional
- (void)doProcesaServer:(BaseResponse *)serverResponse;
@end

@interface ServerProcess : NSObject

-(void)sendDataToServerMultipart:(NSMutableDictionary *) NSMap
                    withJsonData:(NSString *) jSonData
                   withIdProcess:(NSString *) idProceso;

-(void)sendDataToServerMultipartURLConnection:(NSMutableDictionary *) NSMap
                                 withJsonData:(NSString *) jSonData
                                withIdProcess:(NSString *) idProceso
                                  withContext:(NSString *) urlContext;

//Envia informacion MultiPart al Servidor!
-(void)sendDataToServerMultipartURLEndPoint:(NSMutableDictionary *) NSMap
                               withJsonData:(NSString *) jSonData
                              withIdProcess:(NSString *) idProceso
                               withEndPoint:(NSString *) endPoint;

//Envia informacion al Servidor dependiendo del Verbo GET/POST/PUT
-(void)sendDataToServer:(NSString *) jSonData
               withVerb:(NSString *) verb
        withURLEndPoint:(NSString *) sUrl
          withIdProcess:(NSString *) sIdProceso;

//Referencia al Delegado
@property (strong, nonatomic)  id <ProcesaServerDelegate> procesaServerDelegate;

@end

