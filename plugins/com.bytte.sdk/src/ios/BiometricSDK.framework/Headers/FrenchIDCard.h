//
//  FrenchIDCard.h
//  BiometricSDK
//
//  Created by RIBEIRO Tiago on 28/04/17.
//  Copyright © 2017 MORPHO. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <BiometricSDK/MRZRecord.h>

@interface FrenchIDCard : MRZRecord

@property (nonatomic, strong, nullable) NSString *optional;

@end
