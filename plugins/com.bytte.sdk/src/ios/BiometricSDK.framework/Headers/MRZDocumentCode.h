//
//  MRZDocumentCode.h
//  BiometricSDK
//
//  Created by RIBEIRO Tiago on 28/04/17.
//  Copyright © 2017 MORPHO. All rights reserved.
//


typedef NS_ENUM(NSUInteger, MRZDocumentCode) {
    
    MRZDocumentCodePassport = 0,
    MRZDocumentCodeTypeI = 1,
    MRZDocumentCodeTypeA = 2,
    MRZDocumentCodeCrewMember = 3,
    MRZDocumentCodeTypeC = 4,
    MRZDocumentCodeTypeV = 5,
    MRZDocumentCodeMigrant = 6,
    
    MRZDocumentCodeUnknown = -1
};


NSString * __nonnull mrzDocumentCodeToString(MRZDocumentCode code);
MRZDocumentCode mrzDocumentCodeFromString(NSString * __nonnull documentCodeString, NSError * __nullable * __nullable error);
