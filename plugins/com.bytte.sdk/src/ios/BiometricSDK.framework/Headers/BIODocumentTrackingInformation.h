//
//  BIODocumentTrackingInformation.h
//  BiometricSDK
//
//  Created by WASAG Kamil - EXT-REPERIO (MORPHO) on 25/11/2016.
//  Copyright © 2016 MORPHO. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <BiometricSDK/BIODocumentInformation.h>

/**
 *  @author Morpho
 *
 *  @brief Object provides tracking information about document
 */
@interface BIODocumentTrackingInformation : BIODocumentInformation


/**
 *  @author Morpho
 *
 *  @brief Initailizer of object thats return object with points sets from given array.
 *
 *  @param location location of received data
 *  @param arrayOfPoints Array of points (as NSValue) to set if array contains more than 4 object inside they will be drop. If number of points is lower than 4 not all points will be set.
 *  @param relativeSize Size of the image that the points relate to.
 *
 *  @return Object of BIODocumentTrackingInformation with preffiled information from array.
 */
- (instancetype _Nonnull)initWithLocation:(BIODocLocation)location withPoints:(NSArray* _Nonnull)arrayOfPoints relativeToSize:(CGSize)relativeSize;

/**
 *  @author Morpho
 *
 *  @brief First point of document frame.
 */
@property (nonatomic) CGPoint point1;

/**
 *  @author Morpho
 *
 *  @brief Second point of document frame.
 */
@property (nonatomic) CGPoint point2;

/**
 *  @author Morpho
 *
 *  @brief Third point of document frame.
 */
@property (nonatomic) CGPoint point3;

/**
 *  @author Morpho
 *
 *  @brief 4th point of document frame.
 */
@property (nonatomic) CGPoint point4;

/**
 *  @author Morpho
 *
 *  @brief size that the points are related to.
 */
@property (nonatomic) CGSize relativeSize;

@end
