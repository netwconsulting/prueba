//
//  FingerCaptureHandlerDelegate.h
//  BiometricSDK
//
//  Created by RIBEIRO Tiago on 16/03/17.
//  Copyright © 2017 MORPHO. All rights reserved.
//

#import <BiometricSDK/BIOAuthenticationResult.h>
#import <BiometricSDK/BIOReplayProtocol.h>

@protocol FingerCaptureHandlerDelegate <BIOReplayProtocol>

@optional

- (void)captureTracking:(NSArray<BIOTrackingInfo*> * _Nullable)trackingInfo withError:(NSError * _Nullable)error;

- (void)capturedFingers:(NSArray<BIOImage *> * _Nullable)images withError:(NSError * _Nullable)error;

- (void)capturedHand:(BIOImage * _Nullable)image withError:(NSError * _Nullable)error;

- (void)fastAuthenticationFinished:(BIOAuthenticationResult * _Nullable)authenticationResult withError:(NSError * _Nullable)error;

@end
