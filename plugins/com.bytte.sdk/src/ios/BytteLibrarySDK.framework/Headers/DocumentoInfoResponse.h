//
//  DocumentoInfoResponse.h
//  MBiometric
//
//  Created by Allan Diaz on 11/15/16.
//  Copyright © 2016 Allan Diaz. All rights reserved.
//

#ifndef DocumentoInfoResponse_h
#define DocumentoInfoResponse_h
#endif /* DocumentoInfoResponse_h */
#import "BaseResponse.h"
#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface DocumentoInfoResponse : BaseResponse
@property (nonatomic) NSNumber * ScoreObtenido;
@property (nonatomic) BOOL Autenticado;
@property (nonatomic) BOOL Existe;
@property (nonatomic) UIImage  * ImagenProceso;
@property (nonatomic) NSString * IdentificadorProceso;

@property (nonatomic) BOOL UnEnrollFingerPrintStatus;
@property (nonatomic) BOOL UnEnrollFaceStatus;
@property (nonatomic) NSString * UnEnrollFingerprintMessage;
@property (nonatomic) NSString * UnEnrollFaceMessage;

@end


/*
 public bool Autenticado { get; set; }
 public long ScoreObtenido { get; set; }
 */
