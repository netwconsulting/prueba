//
//  DocumentMRZLine.h
//  BiometricSDK
//
//  Created by WASAG Kamil - EXT-REPERIO (MORPHO) on 30/05/2016.
//  Copyright © 2016 WASAG Kamil - EXT-REPERIO (MORPHO). All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DocumentMRZLine : NSObject

@property (nonatomic) int lineNumber;
@property (nonatomic) int consorank;
@property (nonatomic, strong , nonnull) NSString* text;

@end
