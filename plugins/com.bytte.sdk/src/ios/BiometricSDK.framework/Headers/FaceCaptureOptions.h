//
//  BIOFaceCaptureOptions.h
//  BiometricSDK
//
//  Created by RIBEIRO Tiago on 15/03/17.
//  Copyright © 2017 MORPHO. All rights reserved.
//

#import <BiometricSDK/BIOCaptureOptions.h>
#import <BiometricSDK/FaceCaptureMode.h>

@interface FaceCaptureOptions : BIOCaptureOptions

/**
 *  Default initializer.
 *  This initializer set all options for BCBioCaptureModeFaceLivenessMedium, BCCameraFront, BCTorchOFF
 *
 *  @return Instance of BCBioCaptureOptions
 */

- (instancetype _Nonnull)init;

/**
 *  Initialzer that create options for BCBioCaptureMode
 *  This Initialzer set options: BIOCameraFront, BIOTorchOFF, BIOOverlayOFF
 *
 *  @param mode mode in which FaceCaptureHandler will work
 *
 *  @return Instance of FaceCaptureOptions
 */
- (instancetype _Nonnull)initWithMode:(FaceCaptureMode)mode;


/**
 * Initializer that allows to set all options.
 *
 * @param mode mode in which FaceCaptureHandler will work
 * @param camera sets camera front or rear
 * @param torch turn camera flashlight on or off
 * @param overlay turn show or hide overlay of biometrics on preview
 * @param timeout timeout for capturing in seconds
 *
 *@return Instance of FaceCaptureOptions
 */
-(instancetype _Nonnull)initWithMode:(FaceCaptureMode)mode
                 withCamera:(BIOCamera)camera
                 witchTorch:(BIOTorch)torch
                withOverlay:(BIOOverlay)overlay
                withTimeout:(NSTimeInterval)timeout;


/**
 *  Determine in what mode BIOCapture works e.g.: Livens detection high, Iris detection etc.
 */

@property (nonatomic, assign) FaceCaptureMode bioCaptureMode;


/**
 *  @author Morpho
 *
 *  @brief Property that sets the time interval delay between challenges
 */
@property (nonatomic, assign) NSTimeInterval challengeIntervalDelay;


/**
 @author    Morpho
 
 @brief Property that defines the maximum number of captures before locking the capture for a certain delay. (Default: 3)
 */
@property (nonatomic, assign) NSInteger maxCapturesBeforeDelay;


/**
 @author    Morpho
 
 @brief Property that defines the capture delay in milliseconds for each lock without multiplier.
 The calculation of the delay is calculated after. (Default: 1000)
 
 `timeDelay = numberOfLocks^2 * timeCaptureDelay * timeCaptureDelayMultiplier`
 */
@property (nonatomic, assign) double timeCaptureDelay;


/**
 @author    Morpho
 
 @brief Property that defines the capture delay multiplier for each lock without multiplier.
 The calculation of the delay is calculated after. (Default: 4)
 
 `timeDelay = numberOfLocks^2 * timeCaptureDelay * timeCaptureDelayMultiplier`
 */
@property (nonatomic, assign) double timeCaptureDelayMultipler;

@end
