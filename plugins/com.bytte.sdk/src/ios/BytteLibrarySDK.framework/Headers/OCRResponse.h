//
//  OCRResponse.h
//  BytteLibrarySDK
//
//  Created by Allan Diaz on 8/29/17.
//  Copyright © 2017 Allan Diaz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import "BaseResponse.h"
#import "OCRDocumentoFrente.h"
#import "OCRDocumentoReverso.h"

@interface OCRResponse : BaseResponse
    @property (nonatomic) OCRDocumentoFrente  * InformacionFrente;
    @property (nonatomic) OCRDocumentoReverso * InformacionReverso;
@end
