//
//  MRZFormat.h
//  BiometricSDK
//
//  Created by RIBEIRO Tiago on 28/04/17.
//  Copyright © 2017 MORPHO. All rights reserved.
//

#import <Foundation/Foundation.h>

@class MRZRecord;

@interface MRZFormat : NSObject

- (MRZRecord * __nonnull)newRecord;

+ (MRZFormat * __nullable)getMRZFormatForMRZString:(NSString * __nonnull)mrzString withError:(NSError * __nullable * __nullable)error;

+ (MRZFormat * __nonnull)frenchIDCardFormat;
+ (MRZFormat * __nonnull)passportFormat;
+ (MRZFormat * __nonnull)mrtdTd1Format;
+ (MRZFormat * __nonnull)mrtdTd2Format;
+ (MRZFormat * __nonnull)mrVisaAFormat;
+ (MRZFormat * __nonnull)mrVisaBFormat;
+ (MRZFormat * __nonnull)slovakId2x34Format;

@end
