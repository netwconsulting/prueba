//
//  BIODocLocation.h
//  BiometricSDK
//
//  Created by WASAG Kamil - EXT-REPERIO (MORPHO) on 25/11/2016.
//  Copyright © 2016 MORPHO. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef int BIODocLocation;

extern const BIODocLocation BIODocLocationMRZ;
extern const BIODocLocation BIODocLocationDatapage;
extern const BIODocLocation BIODocLocationPhoto;
extern const BIODocLocation BIODocLocationReflect;

extern const BIODocLocation BIODocLocationFull;
extern const BIODocLocation BIODocLocationUnknown;
