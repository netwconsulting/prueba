//
//  BIOOCR.h
//  BiometricSDK
//
//  Created by WASAG Kamil - EXT-REPERIO (MORPHO) on 14/12/2016.
//  Copyright © 2016 MORPHO. All rights reserved.
//

typedef NS_ENUM(int32_t, BIOOCR){
    BIOOCRDisable = 0,
    BIOOCREnable = 1
};
