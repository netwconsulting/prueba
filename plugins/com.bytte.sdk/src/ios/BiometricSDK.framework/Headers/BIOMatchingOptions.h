//
//  BIOMatchingOptions.h
//  BiometricSDK
//
//  Created by WASAG Kamil - EXT-REPERIO (MORPHO) on 05/05/2016.
//  Copyright © 2016 WASAG Kamil - EXT-REPERIO (MORPHO). All rights reserved.
//

#import <Foundation/Foundation.h>
#import <BiometricSDK/BIOModality.h>


/**
 * Object used to configure BIOMatherHandler
 */
@interface BIOMatchingOptions : NSObject


/**
 *  Designated initalizer that set modality of
 *
 *  @param modality Determinate what kind of biometrics will be processing.
 *
 *  @return Instance of BIOMatchingOptions with modality already set
 */
- (instancetype _Nonnull) initWithModality:(BIOModality)modality;

/**
 *  Property that determine which biometrics will be analyse 
 */
@property (nonatomic) BIOModality modality;

@end
