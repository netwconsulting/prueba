//
//  ChileBackViewController.h
//  BytteLibrarySDK
//
//  Created by Allan Diaz on 11/28/17.
//  Copyright © 2017 Allan Diaz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "InfoChileDocumento.h"

@protocol CapturaMRZChileCodeDelegate <NSObject>
@optional
- (void)doCapturaMRZChileCodeDelegate:(InfoChileDocumento *)infoDocumento;
@end
@interface ChileBackViewController : UIViewController

//Init
- (id)initWithKey:(NSString *)Key AndUIImageGuide:(UIImage *) imgGuia WithDet:(UIImage *) imDetalle;
-(void)releaseObjects;

//Referencia al Delegado
@property (strong, nonatomic)  id <CapturaMRZChileCodeDelegate> capturaMRZChileCodeDelegate;
@end
